<?php

namespace backend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\web\Controller;
use common\models\ar\User;
use backend\models\UserSearch;
use backend\models\UserForm;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @return mixed
     * @throws \yii\base\InvalidParamException
     */
    public function actionIndex()
    {
        $searchModel  = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->getRequest()->getQueryParams());

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\base\InvalidParamException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * @return mixed
     * @throws \yii\base\InvalidParamException
     */
    public function actionCreate()
    {
        $model     = new User();
        $formModel = new UserForm();
        $formModel->setModel($model);

        $this->ajaxValidation($formModel);

        if( $formModel->load(Yii::$app->getRequest()->post()) ) {
            if( $formModel->save() ) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                $formModel->flashError();
            }
        }

        return $this->render('create', [
            'formModel' => $formModel,
        ]);
    }

    /**
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\base\InvalidParamException
     */
    public function actionUpdate($id)
    {
        /**
         * @var \backend\models\UserForm $model
         */
        $model     = $this->findModel($id);
        $formModel = new UserForm();
        $formModel->setModel($model);

        $this->ajaxValidation($formModel);

        if( $formModel->load(Yii::$app->getRequest()->post()) ) {
            if( $formModel->save() ) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                $formModel->flashError();
            }
        }

        return $this->render('update', [
            'formModel' => $formModel,
        ]);
    }

    /**
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\base\InvalidParamException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * @param integer $id
     *
     * @return \common\models\ar\User the loaded model
     * @throws \yii\web\NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $model = User::find()->where(['user.id' => (int)$id])->one();

        if( $model === NULL ) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }

        return $model;
    }
}
