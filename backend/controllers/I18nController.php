<?php

namespace backend\controllers;

use yii\web\NotFoundHttpException;
use common\models\ar\MessageSource;
use common\models\ar\MessageTranslation;
use common\web\Controller;
use common\web\Response;
use common\widgets\ActiveForm;
use backend\models\MessageForm;
use backend\models\MessageSearch;

class I18nController extends Controller
{
    public function actionIndex()
    {
        $formModel    = new MessageForm(NULL, []);
        $searchModel  = new MessageSearch();
        $dataProvider = $searchModel->search(\Yii::$app->getRequest()->getQueryParams());

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
            'formModel'    => $formModel,
        ]);
    }

    /**
     * @param int $id
     *
     * @return array
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionView($id)
    {
        \Yii::$app->getResponse()->format = Response::FORMAT_JSON;

        $model        = $this->getModel();
        $messages     = $model->messageTranslations;
        $translations = [];

        foreach( \Yii::$app->getI18n()->languages as $language ) {
            $translations[$language] = isset($messages[$language]->translation) ? $messages[$language]->translation : NULL;
        }

        return [
            'id'           => $id,
            'source'       => $model->message,
            'translations' => $translations,
        ];
    }

    /**
     * @return array
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionUpdate()
    {
        \Yii::$app->getResponse()->format = Response::FORMAT_JSON;

        $model     = $this->getModel();
        $formModel = new MessageForm($model);

        $formModel->load(\Yii::$app->getRequest()->post());
        $formModel->save();

        return [
            'errors'  => $formModel->getFirstErrors(),
            'success' => $formModel->hasErrors() === false,
            'data'    => [],
        ];
    }

    /**
     * @return array
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionValidate()
    {
        \Yii::$app->getResponse()->format = Response::FORMAT_JSON;

        $model     = $this->getModel();
        $formModel = new MessageForm($model);

        $formModel->load(\Yii::$app->getRequest()->post());

        return ActiveForm::validate($formModel);
    }

    public function actionDelete($id)
    {
        \Yii::$app->getResponse()->format = Response::FORMAT_JSON;

        $model = $this->getModel();

        $model->delete();

        $request = \Yii::$app->getRequest()->getQueryParams();

        unset($request['id']);
        $request[0] = 'index';

        return $this->redirect($request);
    }

    /**
     * @return \common\models\ar\MessageSource
     * @throws \yii\web\NotFoundHttpException
     */
    private function getModel()
    {
        /**
         * @var \common\models\ar\MessageSource $model
         */
        $id = isset($_REQUEST['id']) ? $_REQUEST['id'] : NULL;

        if( is_numeric($id) === false ) {
            throw new NotFoundHttpException();
        }

        $model = MessageSource::find()->where(['id' => (int)$id])->one();

        if( $model === NULL ) {
            throw new NotFoundHttpException();
        }

        return $model;
    }
}
