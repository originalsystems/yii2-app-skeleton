<?php

namespace backend\controllers;

use common\web\AuthControllerInterface;
use Yii;
use common\web\Controller;
use backend\models\SignInForm;

class AuthController extends Controller implements AuthControllerInterface
{
    /**
     * Logs in a user.
     *
     * @return mixed
     * @throws \yii\base\InvalidParamException
     */
    public function actionSignIn()
    {
        $model = new SignInForm();

        if( $model->load(Yii::$app->request->post()) ) {
            if( $model->login() ) {
                return $this->goBack();
            } else {
                $model->flashError();
            }
        }

        return $this->render('sign-in', [
            'model' => $model,
        ]);
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionSignOut()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
