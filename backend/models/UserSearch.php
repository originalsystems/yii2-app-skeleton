<?php

namespace backend\models;

use yii\data\ActiveDataProvider;
use common\models\ar\User;

class UserSearch extends User
{
    public function rules()
    {
        $statusLabels = self::statusLabels();

        return [
            [['id'], 'integer'],
            [['status'], 'in', 'range' => array_keys($statusLabels)],
            [['name', 'login', 'email'], 'string'],
        ];
    }

    /**
     * @param array $params
     *
     * @return ActiveDataProvider
     * @throws \yii\base\InvalidParamException
     */
    public function search($params)
    {
        $query = UserSearch::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'  => [
                'defaultOrder' => ['id' => SORT_ASC],
            ],
        ]);

        $this->load($params);

        if( !$this->validate() ) {
            $query->where('0=1');

            return $dataProvider;
        }

        $query->andFilterWhere([
            'user.id'     => $this->id,
            'user.status' => $this->status,
        ]);

        $query->andFilterWhere(['LIKE', 'user.name', $this->name]);
        $query->andFilterWhere(['LIKE', 'user.login', $this->login]);
        $query->andFilterWhere(['LIKE', 'user.email', $this->email]);

        return $dataProvider;
    }
}
