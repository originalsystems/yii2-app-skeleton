<?php

namespace backend\models;

use yii\data\ActiveDataProvider;
use common\models\ar\MessageTranslation;
use common\i18n\I18N;
use common\models\ar\MessageSource;

class MessageSearch extends MessageSource
{
    public $with_translation;
    public $translations = [];

    const WITH_TRANSLATION    = 'with';
    const WITHOUT_TRANSLATION = 'without';

    public function attributeLabels()
    {
        return array_merge(MessageSource::labels(), [
            'with_translation' => \Yii::t('app', 'With translation'),
        ]);
    }

    public static function translationLabels()
    {
        return [
            self::WITH_TRANSLATION    => \Yii::t('app', 'With translation'),
            self::WITHOUT_TRANSLATION => \Yii::t('app', 'Without translation'),
        ];
    }

    public function rules()
    {
        $translationLabels = self::translationLabels();

        return [
            [['message'], 'string'],
            [['category'], 'string', 'max' => 255],
            [['with_translation'], 'in', 'range' => array_keys($translationLabels)],
            [['translations'], 'validateTranslations', 'skipOnEmpty' => false],
        ];
    }

    public function validateTranslations($attribute)
    {
        $this->translations = (array)$this->translations;

        foreach( \Yii::$app->getI18n()->languages as $language ) {
            $this->translations[$language] = isset($this->translations[$language]) ? $this->translations[$language] : NULL;
        }
    }

    public function search(array $data = [])
    {
        $query        = self::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($data);

        if( $this->validate() === false ) {
            $query->where('1=0');

            return $dataProvider;
        }

        $translateTable = MessageTranslation::tableName();

        $query->with(['messageTranslations']);

        foreach( \Yii::$app->getI18n()->languages as $language ) {
            $alias = 'message_' . $language;

            $query->leftJoin($translateTable . " AS \"{$alias}\"", "\"{$alias}\".\"id\" = \"message_source\".\"id\" AND \"{$alias}\".\"language\" = '{$language}'");
            $query->andFilterWhere(['LIKE', "LOWER(\"{$alias}\".\"translation\")", mb_strtolower($this->translations[$language])]);
        }

        switch( $this->with_translation ) {
            case self::WITH_TRANSLATION:
                $condition = ['AND'];

                foreach( \Yii::$app->getI18n()->languages as $language ) {
                    if( $language !== \Yii::$app->sourceLanguage ) {
                        $alias       = 'message_' . $language;
                        $condition[] = "\"{$alias}\".\"translation\" != \"message_source\".\"message\"";
                    }
                }

                $query->andWhere($condition);
                break;

            case self::WITHOUT_TRANSLATION:
                $condition = ['OR'];

                foreach( \Yii::$app->getI18n()->languages as $language ) {
                    if( $language !== \Yii::$app->sourceLanguage ) {
                        $alias       = 'message_' . $language;
                        $condition[] = "\"{$alias}\".\"translation\" = \"message_source\".\"message\"";
                    }
                }

                $query->andWhere($condition);
                break;

            default:
                // nothing to do
        }

        $query->andFilterWhere(['LIKE', 'LOWER("message_source"."message")', mb_strtolower($this->message)]);

        return $dataProvider;
    }
}
