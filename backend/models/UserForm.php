<?php

namespace backend\models;

use Yii;
use yii\base\Exception;
use common\base\FormModel;
use common\rbac\AuthManager;
use common\models\aq\UserQuery;
use common\models\ar\User;

class UserForm extends FormModel
{
    public $login;
    public $status;
    public $email;
    public $name;
    public $password;
    public $password_repeat;
    public $roles = [];

    /**
     * @var \common\models\ar\User
     */
    private $_model;

    /**
     * @return \common\models\ar\User
     */
    public function getModel()
    {
        if( $this->_model === NULL ) {
            $this->_model = new User();
        }

        return $this->_model;
    }

    /**
     * @param \common\models\ar\User $model
     */
    public function setModel(User $model)
    {
        $this->login  = $model->login;
        $this->status = $model->status;
        $this->email  = $model->email;
        $this->name   = $model->name;

        foreach( Yii::$app->getAuthManager()->getRolesByUser($model->id) as $role ) {
            $this->roles[] = $role->name;
        }

        $this->_model = $model;
    }

    public function rules()
    {
        $statusLabels = User::statusLabels();

        return [
            [['status', 'name', 'login', 'email'], 'required'],
            [['status'], 'in', 'range' => array_keys($statusLabels)],
            [['email'], 'email'],
            [
                ['login', 'email'],
                'unique',
                'targetClass' => User::class,
                'filter'      => function (UserQuery $query) {
                    if( $this->getModel()->getIsNewRecord() === false ) {
                        $query->andWhere(['NOT', ['id' => $this->getModel()->id]]);
                    }
                },
            ],
            [
                ['password'],
                'required',
                'when' => function () {
                    return $this->getModel()->getIsNewRecord();
                },
            ],
            [['password'], 'string', 'min' => 6],
            [['password_repeat'], 'compare', 'compareAttribute' => 'password', 'skipOnEmpty' => false],
            [['roles'], 'validateRoles', 'skipOnEmpty' => false],
        ];
    }

    public function validateRoles($attribute)
    {
        $roleLabels  = AuthManager::roleLabels();
        $this->roles = array_filter((array)$this->roles);

        foreach( $this->roles as $role ) {
            if( array_key_exists($role, $roleLabels) === false ) {
                $this->addError($attribute, Yii::t('app', 'Invalid value'));

                return;
            }
        }
    }

    public function attributeLabels()
    {
        return array_merge(User::labels(), [
            'password'        => Yii::t('app', 'Password'),
            'password_repeat' => Yii::t('app', 'Repeat password'),
            'roles'           => Yii::t('app', 'User roles'),
        ]);
    }

    public function save()
    {
        if( $this->validate() === false ) {
            return false;
        }

        $model       = $this->getModel();
        $security    = Yii::$app->getSecurity();
        $auth        = Yii::$app->getAuthManager();
        $transaction = Yii::$app->getDb()->beginTransaction();

        try {
            $model->login  = $this->login;
            $model->status = $this->status;
            $model->email  = $this->email;
            $model->name   = $this->name;

            if( $model->getIsNewRecord() === true ) {
                $model->auth_key     = $security->generateRandomString(32);
                $model->access_token = $security->generateRandomString(32);
            }

            if( !empty($this->password) ) {
                $model->password_hash = $security->generatePasswordHash($this->password);
            }

            $model->strictSave();

            $auth->revokeAll($model->id);

            foreach( $this->roles as $roleName ) {
                $role = $auth->getRole($roleName);

                $auth->assign($role, $model->id);
            }

            if( Yii::$app->getUser()->getId() === $model->id ) {
                Yii::$app->getUser()->logout();
            }

            $transaction->commit();
        } catch( Exception $ex ) {
            $transaction->rollBack();

            return false;
        }

        return true;
    }
}
