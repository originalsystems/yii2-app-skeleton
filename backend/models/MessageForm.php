<?php

namespace backend\models;

use yii\base\Exception;
use yii\base\Model;
use common\models\ar\MessageSource;
use common\models\ar\MessageTranslation;

class MessageForm extends Model
{
    public $translations = [];

    public function __construct(MessageSource $source = NULL, array $config = [])
    {
        if( $source !== NULL ) {
            $this->_source       = $source;
            $this->_translations = $source->messageTranslations;
        }

        parent::__construct($config);
    }

    /**
     * @var \common\models\ar\MessageSource
     */
    private $_source;

    /**
     * @var \common\models\ar\MessageTranslation[]
     */
    private $_translations = [];

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['translations'], 'validateTranslations', 'skipOnEmpty' => false],
        ];
    }

    public function validateTranslations($attribute)
    {
        if( is_array($this->translations) === false ) {
            $this->addError($attribute . '[' . \Yii::$app->sourceLanguage . ']', \Yii::t('app', '{attribute} must be an array.', ['attribute' => $this->getAttributeLabel($attribute)]));

            return;
        }

        foreach( \Yii::$app->getI18n()->languages as $language ) {
            if( isset($this->translations[$language]) === false || $this->translations[$language] === '' ) {
                $this->addError($attribute . '[' . $language . ']', \Yii::t('app', 'Translation for {language} language can not be empty', ['language' => $language]));

                return;
            }
        }
    }

    /**
     * @param string $language
     *
     * @return \common\models\ar\MessageTranslation
     */
    public function getTranslation($language)
    {
        if( isset($this->_translations[$language]) === false ) {
            $this->_translations[$language] = new MessageTranslation([
                'id'          => $this->_source->id,
                'language'    => $language,
                'translation' => $this->_source->message,
            ]);
        }

        return $this->_translations[$language];
    }

    public function save()
    {
        if( $this->validate() === false ) {
            return false;
        }

        /**
         * @var MessageTranslation[] $messages
         */
        $messages    = [];
        $transaction = \Yii::$app->getDb()->beginTransaction();

        foreach( \Yii::$app->getI18n()->languages as $language ) {
            $messages[$language] = $this->getTranslation($language);
        }

        try {
            foreach( $messages as $message ) {
                $message->translation = $this->translations[$message->language];

                $message->strictSave();
            }

            $transaction->commit();
        } catch( Exception $ex ) {
            $transaction->rollBack();

            foreach( $messages as $message ) {
                $this->addErrors($message->getErrors());
            }

            return false;
        }

        return true;
    }
}
