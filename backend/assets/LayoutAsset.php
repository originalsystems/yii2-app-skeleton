<?php

namespace backend\assets;

use yii\web\YiiAsset;
use yii\bootstrap\BootstrapAsset;
use common\web\AssetBundle;
use common\assets\AutosizeAsset;
use common\assets\FontAwesomeAsset;

class LayoutAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl  = '@web';
    public $css      = [
        'public/backend/css/layout.css',
    ];
    public $js       = [
        'public/js/app.js',
    ];
    public $depends  = [
        YiiAsset::class,
        BootstrapAsset::class,
        FontAwesomeAsset::class,
        AutosizeAsset::class,
    ];
}
