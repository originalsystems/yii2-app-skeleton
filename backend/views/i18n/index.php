<?php
/**
 * @var \common\web\View              $this
 * @var \yii\data\ActiveDataProvider  $dataProvider
 * @var \backend\models\MessageSearch $searchModel
 * @var \backend\models\MessageForm   $formModel
 */
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;
use common\grid\ActionColumn;
use common\widgets\ActiveForm;
use backend\models\MessageSearch;

$this->title                   = Yii::t('app', 'Internationalization');
$this->params['breadcrumbs'][] = $this->title;

$request   = Yii::$app->getRequest();
$actionUrl = \yii\helpers\Url::to([NULL, 'sort' => $request->get('sort'), 'page' => $request->get('page')]);
?>
<section class="section-i18n-grid">

    <?php $form = ActiveForm::begin([
        'id'                   => 'i18n-grid-filter',
        'enableAjaxValidation' => false,
        'layout'               => 'default',
        'method'               => 'get',
        'action'               => [NULL],
        'options'              => [
            'data-role'   => 'grid-pjax-filter',
            'data-target' => '#i18n-grid-pjax',
        ],
    ]); ?>

    <div class="general-fields">
        <h3><?= 'Основной поиск'; ?></h3>
        <div class="field-wrapper">
            <?= $form->field($searchModel, 'message')->textInput([
                'autocomplete' => 'off',
            ]); ?>

            <?= $form->field($searchModel, 'category')->dropDownList(Yii::$app->getI18n()->usedCategories(), [
                'autocomplete' => 'off',
                'prompt'       => Yii::t('app', 'Specify one of values'),
            ]); ?>

            <?= $form->field($searchModel, 'with_translation')->dropDownList(MessageSearch::translationLabels(), [
                'prompt' => Yii::t('app', 'Specify one of values'),
            ]); ?>
        </div>
    </div>

    <div class="translation-fields">
        <h3><?= 'Поиск по переводам'; ?></h3>

        <div class="field-wrapper">
            <?php foreach( \Yii::$app->getI18n()->languages as $language ): ?>
                <?= $form->field($searchModel, "translations[{$language}]")->label(mb_strtoupper($language))->textInput([
                    'autocomplete' => 'off',
                ]); ?>
            <?php endforeach; ?>
        </div>
    </div>

    <button type="submit" class="btn btn-primary"><?= Yii::t('app', 'Filter'); ?></button>

    <?php ActiveForm::end(); ?>

    <?php Pjax::begin([
        'id' => 'i18n-grid-pjax',
    ]); ?>

    <?= GridView::widget([
        'id'           => 'i18n-grid',
        'dataProvider' => $dataProvider,
        'columns'      => array_merge([
            'id',
            'category',
            'message',
        ], array_map(function ($language) {
            return [
                'header'    => mb_strtoupper($language),
                'attribute' => "messageTranslations.{$language}.translation",
            ];
        }, Yii::$app->getI18n()->languages), [
            [
                'class'    => ActionColumn::className(),
                'template' => '<div class="btn-group btn-group-sm">{update}{delete}</div>',
                'buttons'  => [
                    'update' => function ($url, MessageSearch $model, $key) {
                        return Html::a('<i class="fa fa-pencil"></i>', $url, [
                            'title'          => Yii::t('yii', 'Update'),
                            'aria-label'     => Yii::t('yii', 'Update'),
                            'class'          => 'btn btn-default tooltips',
                            'data-pjax'      => '0',
                            'data-placement' => 'bottom',
                            'data-role'      => 'edit',
                        ]);
                    },
                    'delete' => function ($url, MessageSearch $model, $key) {
                        $url       = Yii::$app->getRequest()->getQueryParams();
                        $url[0]    = 'delete';
                        $url['id'] = $model->id;

                        return Html::a('<i class="fa fa-trash"></i>', $url, [
                            'title'          => Yii::t('yii', 'Delete'),
                            'aria-label'     => Yii::t('yii', 'Delete'),
                            'class'          => 'btn btn-danger tooltips',
                            'data-confirm'   => Yii::t('yii', 'Are you sure you want to delete this item?'),
                            'data-method'    => 'post',
                            'data-pjax'      => '0',
                            'data-placement' => 'bottom',
                        ]);
                    },
                ],
                'options'  => [
                    'width' => 80,
                ],
            ],
        ]),
    ]); ?>

    <?php Pjax::end(); ?>

    <div class="modal fade" id="request-call-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <?php $form = ActiveForm::begin([
                    'layout'        => 'default',
                    'action'        => ['/i18n/update'],
                    'validationUrl' => ['/i18n/validate'],
                ]); ?>
                <div class="modal-header">
                    <h4><?= Yii::t('app', 'Update translation message'); ?></h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id" data-bind="id">

                    <div class="form-group">
                        <label><?= Yii::t('app', 'Source message'); ?></label>
                        <div class="source-description" data-bind="source"></div>
                    </div>

                    <?php foreach( Yii::$app->getI18n()->languages as $language ): ?>

                        <?= $form->field($formModel, "translations[{$language}]")->label(mb_strtoupper($language))->textarea([
                            'data-bind'     => 'translation',
                            'data-language' => $language,
                            'autocomplete'  => 'off',
                        ]); ?>

                    <?php endforeach; ?>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default" type="button" data-dismiss="modal"><?= Yii::t('app', 'Cancel'); ?></button>
                    <button class="btn btn-primary" type="submit"><?= Yii::t('app', 'Submit'); ?></button>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</section>