<?php
/**
 * @var \common\web\View            $this
 * @var \yii\bootstrap\ActiveForm   $form
 * @var \frontend\models\SignInForm $model
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title         = Yii::t('app', 'Sign in');
$this->breadcrumbs[] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <p><?= Yii::t('app', 'Fill in your login details for sign in.') ?></p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

            <?= $form->field($model, 'login')->textInput(); ?>

            <?= $form->field($model, 'password')->passwordInput(); ?>

            <?= $form->field($model, 'rememberMe')->checkbox(); ?>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
