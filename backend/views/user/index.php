<?php
/**
 * @var \common\web\View             $this
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var \backend\models\UserSearch   $searchModel
 */

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;
use common\grid\ActionColumn;

?>
<section class="section-user-index">
    <p>
        <?= Html::a('<i class="fa fa-plus"></i> ' . Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']); ?>
    </p>

    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            'id',
            [
                'attribute' => 'status',
                'value'     => 'statusLabel',
            ],
            'name',
            'email',
            'login',
            'updated_at:datetime',
            [
                'class'   => ActionColumn::class,
                'options' => [
                    'width' => 120,
                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</section>
