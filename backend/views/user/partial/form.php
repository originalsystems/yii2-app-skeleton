<?php
/**
 * @var \common\web\View         $this
 * @var \backend\models\UserForm $formModel
 */

use yii\helpers\Html;
use kartik\select2\Select2;
use common\models\ar\User;
use common\rbac\AuthManager;
use common\widgets\ActiveForm;

?>
<section class="section-user-form">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($formModel, 'login')->textInput([
        'maxlength'    => true,
        'autocomplete' => 'off',
    ]); ?>

    <?= $form->field($formModel, 'status')->dropDownList(User::statusLabels(), [
        'autocomplete' => 'off',
        'prompt'       => Yii::t('app', 'Specify one of values'),
    ]); ?>

    <?= $form->field($formModel, 'email')->textInput([
        'maxlength'    => true,
        'autocomplete' => 'off',
    ]); ?>

    <?= $form->field($formModel, 'name')->textInput([
        'maxlength'    => true,
        'autocomplete' => 'off',
    ]); ?>

    <?= $form->field($formModel, 'roles')->widget(Select2::class, [
        'data'          => AuthManager::roleLabels(),
        'options'       => [
            'multiple' => true,
            'prompt'   => Yii::t('app', 'Choose user roles'),
        ],
        'pluginOptions' => [
            'allowClear' => true,
        ],
    ]); ?>

    <?= $form->field($formModel, 'password')->passwordInput([
        'maxlength'    => true,
        'autocomplete' => 'off',
    ]); ?>

    <?= $form->field($formModel, 'password_repeat')->passwordInput([
        'maxlength'    => true,
        'autocomplete' => 'off',
    ]); ?>

    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-6">

            <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']); ?>

        </div>
    </div>

    <?php ActiveForm::end(); ?>
</section>
