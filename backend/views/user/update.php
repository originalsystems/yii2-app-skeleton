<?php
/**
 * @var common\web\View         $this
 * @var backend\models\UserForm $formModel
 */

$this->title = Yii::t('app', 'Update') . ': ' . $formModel->name;

$this->breadcrumbs[] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->breadcrumbs[] = $this->title;
?>
<section class="section-user-update">
    <div class="container">
        <?= $this->render('partial/form', [
            'formModel' => $formModel,
        ]); ?>
    </div>
</section>