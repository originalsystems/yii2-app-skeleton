<?php
/**
 * @var \common\web\View       $this
 * @var \common\models\ar\User $model
 */
use yii\helpers\Html;

?>
<section class="section-user-view">
    <p>
        <?= Html::a('<i class="fa fa-edit"></i> ' . Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']); ?>
        <?= Html::a('<i class="fa fa-trash"></i> ' . Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data'  => [
                'confirm' => Yii::t('app', 'Do you really want delete current record?'),
                'method'  => 'post',
            ],
        ]); ?>
    </p>

    <?= \yii\widgets\DetailView::widget([
        'model'      => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'status',
                'value'     => $model->getStatusLabel(),
            ],
            'name',
            'login',
            'email:email',
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]); ?>
</section>
