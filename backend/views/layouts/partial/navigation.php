<?php
/**
 * @var \common\web\View $this
 */

use common\i18n\I18N;
use common\widgets\LanguageSwitcher;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;

$cID  = Yii::$app->controller->id;
$menu = [
    [
        'label' => Yii::t('app', 'Sign in'),
        'url'   => ['/auth/sign-in'],
    ],
];

if( Yii::$app->getUser()->getIsGuest() === false ) {
    $menu = [
        [
            'label'  => Yii::t('app', 'Main'),
            'url'    => ['/site/index'],
            'active' => $cID === 'site',
        ],
        [
            'label'  => Yii::t('app', 'Users'),
            'url'    => ['/user/index'],
            'active' => $cID === 'user',
        ],
        [
            'label'  => Yii::t('app', 'Internationalization'),
            'url'    => ['/i18n/index'],
            'active' => $cID === 'i18n',
        ],
        LanguageSwitcher::widget(),
        [
            'label'       => Yii::t('app', 'Sign out'),
            'url'         => ['/auth/sign-out'],
            'linkOptions' => ['data-method' => 'post'],
        ],
    ];
}

NavBar::begin([
    'brandLabel' => Yii::$app->name,
    'brandUrl'   => Yii::$app->getHomeUrl(),
    'options'    => [
        'class' => 'navbar-inverse navbar-fixed-top',
    ],
]);

echo Nav::widget([
    'options'      => ['class' => 'navbar-nav navbar-right'],
    'items'        => $menu,
    'encodeLabels' => false,
]);

NavBar::end();
