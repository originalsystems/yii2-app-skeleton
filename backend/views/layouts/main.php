<?php
/**
 * @var \common\web\View $this
 * @var string           $content
 */

use backend\assets\LayoutAsset;
use common\widgets\Alert;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

LayoutAsset::register($this);
$this->registerRelatedFiles('/public/backend/#MODULE_PATH#/#EXTENSION#/#CONTROLLER_ID#.#EXTENSION#');
?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language; ?>">
<head>
    <meta charset="<?= Yii::$app->charset; ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?= Html::csrfMetaTags(); ?>

    <?php $this->head(); ?>

    <title><?= Html::encode($this->title ? : Yii::$app->name); ?></title>
</head>
<body>
<?php $this->beginBody(); ?>

<div class="page-wrapper">
    <header>
        <?= $this->render('partial/navigation'); ?>
    </header>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => $this->breadcrumbs,
        ]); ?>
        <?= Alert::widget(); ?>
        <?= $content; ?>
    </div>
</div>

<footer>
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y'); ?></p>

        <p class="pull-right"><?= Yii::powered(); ?></p>
    </div>
</footer>

<?php $this->endBody(); ?>
</body>
</html>
<?php $this->endPage(); ?>
