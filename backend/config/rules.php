<?php
/**
 * Backend url manager rules
 *
 * @TODO replace f**king slashes with nginx redirects
 */

return [
    '<_lang:(\w\w)>'                                           => 'site/index',
    '<_lang:(\w\w)>/?'                                         => 'site/index',
    '<_lang:(\w\w)>/<controller:[\d\w-]+>'                     => '<controller>',
    '<_lang:(\w\w)>/<controller:[\d\w-]+>/?'                   => '<controller>',
    '<_lang:(\w\w)>/<controller:[\d\w-]+>/<action:[\d\w-]+>'   => '<controller>/<action>',
    '<_lang:(\w\w)>/<controller:[\d\w-]+>/<action:[\d\w-]+>/?' => '<controller>/<action>',
];
