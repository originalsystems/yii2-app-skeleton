<?php
/**
 * Main backend configuration file
 */

return \yii\helpers\ArrayHelper::merge(require APP_ROOT . '/common/config/main.php', [
    'id'                  => 'app-backend',
    'basePath'            => dirname(__DIR__),
    'bootstrap'           => ['log'],
    'controllerNamespace' => 'backend\controllers',
    'components'          => [
        'assetManager' => [
            'class'           => \common\web\AssetManager::class,
            'appendTimestamp' => true,
            'compressCss'     => YII_ENV_PROD,
            'compressJs'      => YII_ENV_PROD,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'log'          => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'  => \yii\log\FileTarget::class,
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'response'     => [
            'class' => \common\web\Response::class,
        ],
        'request'      => [
            'class' => \common\web\Request::class,
        ],
        'view'         => [
            'class' => \common\web\View::class,
        ],
        'urlManager'   => [
            'enablePrettyUrl' => true,
            'showScriptName'  => false,
            'rules'           => require __DIR__ . '/rules.php',
        ],
        'user'         => [
            'class'           => \common\web\User::class,
            'identityClass'   => \common\models\ar\User::class,
            'loginUrl'        => ['/auth/sign-in'],
            'enableAutoLogin' => true,
        ],
    ],
    'as access'           => [
        'class'  => \yii\filters\AccessControl::class,
        'except' => ['site/error', 'auth/sign-in', 'auth/sign-out'],
        'rules'  => [
            [
                'allow' => true,
                'roles' => [\common\rbac\AuthManager::ROLE_ADMIN],
            ],
        ],
    ],
], file_exists(__DIR__ . '/main.local.php') ? require __DIR__ . '/main.local.php' : []);
