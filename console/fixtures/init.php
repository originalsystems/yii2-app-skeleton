<?php

/**
 * @DESCRIPTION Init fixture for creating base users, remove all users in database
 * @SORT        1
 */

use yii\helpers\ArrayHelper;
use yii\helpers\Console;
use common\models\ar\User;
use common\rbac\AuthManager;

if( Console::confirm('Current fixture delete all user. Confirm?') === false ) {
    Yii::$app->end();

    return;
}

$security = Yii::$app->getSecurity();

User::deleteAll();

return ArrayHelper::merge([
    'users' => [
        'class' => User::className(),
        'data'  => [
            [
                'status'        => User::STATUS_ACTIVE,
                'name'          => 'admin',
                'login'         => 'admin',
                'email'         => 'admin@admin.com',
                'auth_key'      => $security->generateRandomString(32),
                'password_hash' => $security->generatePasswordHash('password'),
                'access_token'  => $security->generateRandomString(16),
                '@meta'         => [
                    'roles' => [AuthManager::ROLE_ADMIN],
                ],
            ],
        ],
    ],
], file_exists(__DIR__ . '/init.local.php') ? require __DIR__ . '/init.local.php' : []);
