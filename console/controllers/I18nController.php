<?php

namespace console\controllers;

use Yii;
use common\console\Controller;
use yii\helpers\FileHelper;
use yii\i18n\DbMessageSource;

class I18nController extends Controller
{
    public function actionUpdateDb()
    {
        foreach( Yii::$app->getI18n()->languages as $language ) {
            $dir   = Yii::getAlias("@common/i18n/messages/{$language}");
            $files = glob($dir . '/app*.php');

            foreach( $files as $file ) {
                $category = str_replace('.php', '', basename($file));

                /**
                 * @var array $translations
                 */
                $translations = require $file;

                foreach( $translations as $source => $translation ) {
                    Yii::$app->i18n->addTranslation($language, $source, $translation, $category);
                }
            }
        }
    }

    public function actionUpdateFiles()
    {
        /** @var DbMessageSource $messageSource */
        $messageSource    = Yii::$app->getI18n()->getMessageSource('app*');
        $tableSource      = $messageSource->sourceMessageTable;
        $tableTranslation = $messageSource->messageTable;

        $sql = <<<SQL
SELECT {$tableSource}."message", {$tableSource}."category", {$tableTranslation}."translation", {$tableTranslation}."language"
FROM {$tableSource}
INNER JOIN {$tableTranslation} ON {$tableTranslation}."id" = {$tableSource}."id"
ORDER BY {$tableTranslation}."id" ASC, {$tableTranslation}."language" DESC;
SQL;

        $rows         = Yii::$app->getDb()->createCommand($sql)->queryAll();
        $translations = [];

        foreach( $rows as $row ) {
            $message     = $row['message'];
            $category    = $row['category'];
            $translation = $row['translation'];
            $language    = $row['language'];

            $translations[$language][$category][$message] = $translation;
        }

        foreach( $translations as $language => $categories ) {
            $dir = Yii::getAlias("@common/i18n/messages/{$language}");
            FileHelper::createDirectory($dir);

            foreach( $categories as $category => $messages ) {
                $file = $dir . '/' . $category . '.php';

                file_put_contents($file, "<?php\n\nreturn " . var_export($messages, true) . ';' . PHP_EOL);
            }
        }
    }
}
