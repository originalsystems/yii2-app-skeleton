<?php

namespace console\controllers;

use common\rbac\AuthManager;
use Yii;
use yii\base\InvalidValueException;
use yii\console\Exception;
use yii\db\ActiveRecord;
use common\console\Controller;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;
use yii\helpers\FileHelper;

/**
 * Fill application databases with required data
 *
 * @package console\controllers
 */
class DataController extends Controller
{
    public $defaultAction = 'list';

    public $short = false;

    public function options($actionID)
    {
        return ['color', 'interactive', 'help', 'short'];
    }

    /**
     * Apply application data from fixture configuration files
     * @see /common/fixture/ directory
     *
     * @param $name
     *
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidValueException
     * @throws \yii\db\Exception
     * @throws \yii\base\InvalidParamException
     */
    public function actionApply($name)
    {
        $transaction = Yii::$app->getDb()->beginTransaction();

        try {
            $this->applyFixture($name);

            $transaction->commit();
        } catch( \yii\base\Exception $ex ) {
            $transaction->rollBack();
            throw $ex;
        }
    }

    /**
     * Apply all fixtures in order, based on @SORT parameter
     * @throws \yii\base\Exception
     */
    public function actionAll()
    {
        $fixtures    = $this->fixtures();
        $transaction = Yii::$app->getDb()->beginTransaction();

        try {
            foreach( $fixtures as list( $sort, $name, $description ) ) {
                $this->applyFixture($name);
            }

            $transaction->commit();
        } catch( \yii\base\Exception $ex ) {
            $transaction->rollBack();
            throw $ex;
        }
    }

    /**
     * Print list of available fixtures
     * @throws \yii\base\InvalidParamException
     */
    public function actionList()
    {
        $fixtures = $this->fixtures();

        if( $this->short ) {
            foreach( $fixtures as list( $sort, $name, $description ) ) {
                $this->stdout($name . PHP_EOL);
            }

            return;
        }

        $sortLength = max(array_map(function ($row) {
            return strlen($row[0]);
        }, $fixtures));
        $nameLength = max(array_map(function ($row) {
            return strlen($row[1]);
        }, $fixtures));

        $this->stdout(PHP_EOL);
        $this->stdout('FIXTURES', Console::BOLD);
        $this->stdout(PHP_EOL);
        $this->stdout(PHP_EOL);

        foreach( $fixtures as list( $sort, $name, $description ) ) {
            $position = $sort === NULL ? '[' . str_repeat('#', $sortLength) . ']' : '[' . str_pad($sort, $sortLength, ' ', STR_PAD_LEFT) . ']';
            $position = $this->ansiFormat($position, Console::FG_CYAN);
            $name     = str_pad($name, $nameLength, ' ', STR_PAD_RIGHT);
            $name     = $this->ansiFormat($name, Console::FG_YELLOW);

            $this->stdout("- {$name} {$position} {$description}");
            $this->stdout(PHP_EOL);
        }

        $this->stdout(PHP_EOL);
        $this->stdout('Fixtures placed in order, based on @SORT parameter, placed in fixture phpdoc');

        $this->stdout(PHP_EOL);
        $this->stdout('For apply specific fixture, enter:');
        $this->stdout(PHP_EOL);
        $this->stdout(PHP_EOL);
        $this->stdout('  yii ' . $this->ansiFormat('data/apply', Console::FG_YELLOW) . ' ' . $this->ansiFormat('<fixture>', Console::FG_CYAN));
        $this->stdout(PHP_EOL);
        $this->stdout(PHP_EOL);
    }

    private function fixtures()
    {
        $dir   = Yii::getAlias('@console/fixtures');
        $files = FileHelper::findFiles($dir, [
            'except' => ['*.local.php', '*.local.default.php'],
        ]);

        $fixtures = [];

        foreach( $files as $file ) {
            if( preg_match('#/([^/]+)\.php$#', $file, $matches) ) {
                $content     = file_get_contents($file);
                $name        = $matches[1];
                $sort        = preg_match('#@SORT\s+(\d+)#', $content, $m) ? (int)$m[1] : NULL;
                $description = preg_match('#@DESCRIPTION\s+(.+)#', $content, $m) ? $m[1] : NULL;

                $fixtures[] = [$sort, $name, $description];
            }
        }

        usort($fixtures, function ($a, $b) {
            if( $a[0] === NULL && $b[0] === NULL ) {
                return 0;
            } elseif( $a[0] === NULL ) {
                return 1;
            } elseif( $b[0] === NULL ) {
                return -1;
            }

            $aSort = (int)$a[0];
            $bSort = (int)$b[0];

            if( $aSort === $bSort ) {
                return 0;
            }

            return $aSort < $bSort ? -1 : 1;
        });

        return $fixtures;
    }

    private function applyFixture($name)
    {
        $file = Yii::getAlias("@console/fixtures/{$name}.php");

        if( file_exists($file) === false ) {
            throw new InvalidValueException('Fixture with such name is not exists');
        }

        $config = Console::timeout("fetching fixture '{$name}' configuration", function () use ($file) {
            return require $file;
        });

        foreach( $config as $title => $collection ) {
            Console::timeout("work on {$title}", [Console::class, 'build'], [$collection]);
        }
    }
}
