<?php

use yii\db\Migration;
use common\rbac\AuthManager;

class m160000_000010_rbac extends Migration
{
    public function safeUp()
    {
        $auth       = Yii::$app->getAuthManager();
        $roleLabels = AuthManager::roleLabels();

        foreach( $roleLabels as $name => $label ) {
            $role = $auth->createRole($name);

            $role->description = $label;

            $auth->add($role);
        }
    }

    public function safeDown()
    {
        $auth = Yii::$app->getAuthManager();

        $auth->removeAll();
    }
}
