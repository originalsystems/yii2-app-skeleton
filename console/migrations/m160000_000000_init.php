<?php

use yii\helpers\Console;

class m160000_000000_init extends \common\db\pgsql\Migration
{
    public $tables = [
        '"public"."file"',
        '"public"."image"',
        '"public"."user"',
        '"public"."auth_rule"',
        '"public"."auth_item"',
        '"public"."auth_item_child"',
        '"public"."auth_assignment"',
    ];

    public function upTables()
    {
        // --- "public"."user"
        $table = '"public"."user"';
        $sql   = <<<SQL
CREATE TABLE {$table} (
    "id"                   SERIAL4      NOT NULL,
    "status"               VARCHAR(16)  NOT NULL,
    "name"                 VARCHAR(255) NOT NULL,
    "login"                VARCHAR(255) NOT NULL UNIQUE,
    "email"                VARCHAR(255) NOT NULL UNIQUE,
    "auth_key"             VARCHAR(32)  NOT NULL,
    "password_hash"        VARCHAR(60)  NOT NULL,
    "password_reset_token" VARCHAR(60)  NULL UNIQUE,
    "access_token"         VARCHAR(32)  NOT NULL UNIQUE,
    "created_at"           TIMESTAMPTZ  DEFAULT CURRENT_TIMESTAMP,
    "updated_at"           TIMESTAMPTZ,
    PRIMARY KEY ("id")
)
SQL;
        $this->execute($sql);

        // --- "public"."auth_rule"
        $table = '"public"."auth_rule"';
        $sql   = <<<SQL
CREATE TABLE {$table} (
    "name"       VARCHAR(64) NOT NULL,
    "data"       TEXT,
    "created_at" INTEGER,
    "updated_at" INTEGER,
    PRIMARY KEY ("name")
);
SQL;
        $this->execute($sql);

        // --- "public"."auth_item"
        $table = '"public"."auth_item"';
        $sql   = <<<SQL
CREATE TABLE {$table} (
    "name"        VARCHAR(64)  NOT NULL,
    "type"        INTEGER      NOT NULL,
    "description" TEXT,
    "rule_name"   VARCHAR(64),
    "data"        TEXT,
    "created_at"  INTEGER,
    "updated_at"  INTEGER,
    PRIMARY KEY ("name"),
    FOREIGN KEY ("rule_name") REFERENCES "public"."auth_rule" ("name") ON UPDATE SET NULL ON DELETE CASCADE
);
SQL;
        $this->execute($sql);

        // --- "public"."auth_item_child"
        $table = '"public"."auth_item_child"';
        $sql   = <<<SQL
CREATE TABLE {$table} (
    "parent" VARCHAR(64) NOT NULL,
    "child"  VARCHAR(64) NOT NULL,
    PRIMARY KEY ("parent", "child"),
    FOREIGN KEY ("parent") REFERENCES "public"."auth_item" ("name") ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY ("child")  REFERENCES "public"."auth_item" ("name") ON UPDATE CASCADE ON DELETE CASCADE
);
SQL;
        $this->execute($sql);

        // --- "public"."auth_assignment"
        $table = '"public"."auth_assignment"';
        $sql   = <<<SQL
CREATE TABLE {$table} (
    "item_name"  VARCHAR(64) NOT NULL,
    "user_id"    INT4        NOT NULL,
    "created_at" INTEGER,
    PRIMARY KEY ("item_name", "user_id"),
    FOREIGN KEY ("item_name") REFERENCES "public"."auth_item" ("name") ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY ("user_id")   REFERENCES "public"."user" ("id") ON UPDATE CASCADE ON DELETE CASCADE
);
SQL;
        $this->execute($sql);

        // --- "public"."file"
        $table = '"public"."file"';
        $sql   = <<<SQL
CREATE TABLE {$table} (
    "id"            SERIAL4      NOT NULL,
    "content_type"  VARCHAR(128) NOT NULL,
    "original_name" VARCHAR(255) NOT NULL,
    "path"          VARCHAR(512) NOT NULL,
    "created_at"    TIMESTAMPTZ  DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY ("id")
);
SQL;
        $this->execute($sql);

        // --- "public"."image"
        $table = '"public"."image"';
        $sql   = <<<SQL
CREATE TABLE {$table} (
    PRIMARY KEY ("id")
) INHERITS ("public"."file");
SQL;
        $this->execute($sql);
    }

    public function upIndexes()
    {
        $this->execute('CREATE INDEX "auth_item_idx_rule_name_fkey" ON "public"."auth_item" USING BTREE ("rule_name")');
        $this->execute('CREATE INDEX "user_idx_status" ON "public"."user" USING HASH ("status")');
    }

    public function upExtra()
    {
        Console::upStatTriggers();
        Console::createStatTriggers($this->tables);
    }

    public function downExtra()
    {
        Console::downStatTriggers();
    }
}
