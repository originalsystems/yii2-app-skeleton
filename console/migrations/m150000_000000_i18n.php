<?php

use common\db\pgsql\Migration;

class m150000_000000_i18n extends Migration
{
    public $tables = [
        '"public"."message_source"',
        '"public"."message_translation"',
    ];

    public function upTables()
    {
        // --- "public"."message_source"
        $table = '"public"."message_source"';
        $sql   = <<<SQL
CREATE TABLE {$table} (
    "id"       SERIAL4 NOT NULL,
    "category" VARCHAR(255),
    "message"  TEXT
)
SQL;
        $this->execute($sql);

        // --- "public"."message_translation"
        $table = '"public"."message_translation"';
        $sql   = <<<SQL
CREATE TABLE {$table} (
    "id"          INT4        NOT NULL,
    "language"    VARCHAR(16) NOT NULL,
    "translation" TEXT
)
SQL;
        $this->execute($sql);
    }

    public function upIndexes()
    {
        $this->execute('ALTER TABLE "message_source" ADD CONSTRAINT "message_source_pkey" PRIMARY KEY ("id")');
        $this->execute('ALTER TABLE "message_translation" ADD CONSTRAINT "message_translation_pkey" PRIMARY KEY ("id", "language")');
        $this->execute('ALTER TABLE "message_translation" ADD CONSTRAINT "message_translation_message_source_id_fkey" FOREIGN KEY ("id") REFERENCES "public"."message_source" ("id") ON UPDATE CASCADE ON DELETE CASCADE');

        $this->execute('CREATE INDEX "message_translation_idx_language" ON "public"."message_translation" USING BTREE ("language")');
        $this->execute('ALTER TABLE "public"."message_translation" CLUSTER ON "message_translation_idx_language"');
        $this->execute('CREATE INDEX "message_source_idx_category" ON "public"."message_source" USING BTREE ("category")');
        $this->execute('ALTER TABLE "public"."message_source" CLUSTER ON "message_source_idx_category"');
    }
}
