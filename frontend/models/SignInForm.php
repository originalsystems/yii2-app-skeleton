<?php
namespace frontend\models;

use Yii;
use common\base\FormModel;
use common\models\ar\User;

/**
 * Sign in form
 */
class SignInForm extends FormModel
{
    public $login;
    public $password;
    public $rememberMe = true;

    public function rules()
    {
        return [
            [['login', 'password'], 'required'],
            [['rememberMe'], 'boolean'],
            [['password'], 'validatePassword'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'login'      => Yii::t('app', 'Login'),
            'password'   => Yii::t('app', 'Password'),
            'rememberMe' => Yii::t('app', 'Remember me'),
        ];
    }

    public function validatePassword($attribute, $params)
    {
        if( $this->hasErrors() ) {
            return false;
        }

        $user = $this->getUser();

        if( $user === NULL ) {
            $this->addError($attribute, Yii::t('app', 'Invalid login or password.'));

            return false;
        }

        try {
            $validPassword = Yii::$app->getSecurity()->validatePassword($this->password, $user->password_hash);
        } catch( \Exception $ex ) {
            $validPassword = false;
        }

        if( $validPassword === false ) {
            $this->addError($attribute, Yii::t('app', 'Invalid login or password.'));

            return false;
        }

        return true;
    }

    /**
     * @return boolean
     * @throws \yii\base\InvalidParamException
     */
    public function login()
    {
        if( $this->validate() === false ) {
            return false;
        }

        /**
         * Log in on month in case of checked "rememberMe"
         */
        return Yii::$app->getUser()->login($this->getUser(), $this->rememberMe ? 2592000 : 0);
    }

    /**
     * @var User
     */
    private $_user = false;

    /**
     * @return User|null
     */
    protected function getUser()
    {
        if( $this->_user === false ) {
            $this->_user = User::find()->where(['login' => $this->login, 'status' => User::STATUS_ACTIVE])->one();
        }

        return $this->_user;
    }
}
