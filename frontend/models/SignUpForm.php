<?php

namespace frontend\models;

use Yii;
use common\base\FormModel;
use common\models\ar\User;

/**
 * Signup form
 */
class SignUpForm extends FormModel
{
    public $name;
    public $login;
    public $email;
    public $password;

    public function rules()
    {
        return [
            ['name', 'filter', 'filter' => 'trim'],
            ['name', 'required'],
            ['name', 'string', 'min' => 2, 'max' => 255],

            ['login', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => User::class, 'message' => 'This login has already been taken.'],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => User::class, 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name'     => Yii::t('app', 'Name'),
            'login'    => Yii::t('app', 'Login'),
            'email'    => Yii::t('app', 'E-mail'),
            'password' => Yii::t('app', 'Password'),
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     * @throws \yii\base\InvalidParamException
     * @throws \yii\base\Exception
     */
    public function signup()
    {
        $security = Yii::$app->getSecurity();

        if( $this->validate() === false ) {
            return NULL;
        }

        $user                = new User();
        $user->status        = User::STATUS_ACTIVE;
        $user->name          = $this->name;
        $user->login         = $this->login;
        $user->email         = $this->email;
        $user->password_hash = $security->generatePasswordHash($this->password);
        $user->auth_key      = $security->generateRandomString(32);
        $user->access_token  = $security->generateRandomString(16);

        if( $user->save() ) {
            return $user;
        }

        return NULL;
    }
}
