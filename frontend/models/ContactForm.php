<?php

namespace frontend\models;

use common\base\FormModel;
use Yii;
use yii\base\Model;
use common\mixin\FormModelTrait;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends FormModel
{
    public $name;
    public $email;
    public $subject;
    public $body;
    public $verifyCode;

    public function rules()
    {
        return [
            [['name', 'email', 'subject', 'body'], 'required'],
            [['email'], 'email'],
            [['verifyCode'], 'captcha'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name'       => Yii::t('app', 'Name'),
            'email'      => Yii::t('app', 'E-mail'),
            'subject'    => Yii::t('app', 'Subject'),
            'body'       => Yii::t('app', 'Body'),
            'verifyCode' => Yii::t('app', 'Verification Code'),
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param  string $email the target email address
     *
     * @return boolean whether the email was sent
     */
    public function sendEmail($email)
    {
        return Yii::$app->mailer->compose()
            ->setTo($email)
            ->setFrom([$this->email => $this->name])
            ->setSubject($this->subject)
            ->setTextBody($this->body)
            ->send();
    }
}
