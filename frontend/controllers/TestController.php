<?php

namespace frontend\controllers;

use common\models\ar\MessageTranslation;
use common\web\Controller;

class TestController extends Controller
{
    public function actionIndex()
    {
        $source = MessageTranslation::find()->one();

        var_dump($source->messageSource->messageTranslations);
    }
}
