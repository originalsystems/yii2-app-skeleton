<?php

namespace frontend\controllers;

use Yii;
use common\web\Controller;
use common\web\AuthControllerInterface;
use frontend\models\SignInForm;
use frontend\models\SignUpForm;

class AuthController extends Controller implements AuthControllerInterface
{
    /**
     * Logs in a user.
     *
     * @return mixed
     * @throws \yii\base\InvalidParamException
     */
    public function actionSignIn()
    {
        $model = new SignInForm();

        if( $model->load(Yii::$app->request->post()) ) {
            if( $model->login() ) {
                return $this->goBack();
            } else {
                $model->flashError();
            }
        }

        return $this->render('sign-in', [
            'model' => $model,
        ]);
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionSignOut()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Signs user up.
     *
     * @return mixed
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidParamException
     */
    public function actionSignUp()
    {
        $model = new SignUpForm();

        if( $model->load(Yii::$app->request->post()) ) {
            if( $user = $model->signup() ) {
                if( Yii::$app->getUser()->login($user) ) {
                    return $this->goHome();
                }
            } else {
                $model->flashError();
            }
        }

        return $this->render('sign-up', [
            'model' => $model,
        ]);
    }
}
