<?php

namespace frontend\controllers;

use common\web\Controller;
use frontend\models\ContactForm;
use Yii;
use yii\captcha\CaptchaAction;
use yii\web\ErrorAction;

/**
 * Site controller
 */
class SiteController extends Controller
{
    public function actions()
    {
        return [
            'error'   => [
                'class' => ErrorAction::class,
            ],
            'captcha' => [
                'class'           => CaptchaAction::class,
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : NULL,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     * @throws \yii\base\InvalidParamException
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     * @throws \yii\base\InvalidParamException
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if( $model->load(Yii::$app->request->post()) && $model->validate() ) {
            if( $model->sendEmail(Yii::$app->params['admin-email']) ) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending email.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     * @throws \yii\base\InvalidParamException
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
