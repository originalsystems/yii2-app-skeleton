<?php
/**
 * @var \common\web\View $this
 */

use common\widgets\LanguageSwitcher;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;

if( Yii::$app->getUser()->getIsGuest() ) {
    $menu = [
        LanguageSwitcher::widget(),
        [
            'label' => Yii::t('app', 'Sign up'),
            'url'   => ['/auth/sign-up'],
        ],
        [
            'label' => Yii::t('app', 'Sign in'),
            'url'   => ['/auth/sign-in'],
        ],
    ];
} else {
    $menu = [
        [
            'label' => Yii::t('app', 'Main'),
            'url'   => ['/site/index'],
        ],
        [
            'label' => Yii::t('app', 'About'),
            'url'   => ['/site/about'],
        ],
        [
            'label' => Yii::t('app', 'Contacts'),
            'url'   => ['/site/contact'],
        ],
        LanguageSwitcher::widget(),
        [
            'label'       => Yii::t('app', 'Sign out'),
            'url'         => ['/auth/sign-out'],
            'linkOptions' => ['data-method' => 'post'],
        ],
    ];
}

NavBar::begin([
    'brandLabel' => Yii::$app->name,
    'brandUrl'   => Yii::$app->getHomeUrl(),
    'options'    => [
        'class' => 'navbar-inverse navbar-fixed-top',
    ],
]);

echo Nav::widget([
    'options'      => ['class' => 'navbar-nav navbar-right'],
    'items'        => $menu,
    'encodeLabels' => false,
]);

NavBar::end();
