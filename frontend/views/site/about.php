<?php

/**
 * @var \common\web\View $this
 */

use yii\helpers\Html;

$this->title                   = Yii::t('app', 'About');
$this->breadcrumbs[] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>
    <p><?= Yii::t('yii', '(not set)'); ?></p>
</div>
