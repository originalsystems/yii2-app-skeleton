<?php

/**
 * @var \common\web\View            $this
 * @var \yii\bootstrap\ActiveForm   $form
 * @var \frontend\models\SignUpForm $model
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title                   = Yii::t('app', 'Sign up');
$this->breadcrumbs[] = $this->title;
?>
<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>

    <p><?= Yii::t('app', 'The following fields are required for registration of a new user.'); ?></p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

            <?= $form->field($model, 'name')->textInput([
                'autocomplete' => 'off',
            ]); ?>

            <?= $form->field($model, 'login')->textInput([
                'autocomplete' => 'off',
            ]); ?>

            <?= $form->field($model, 'email')->textInput([
                'autocomplete' => 'off',
            ]); ?>

            <?= $form->field($model, 'password')->passwordInput([
                'autocomplete' => 'off',
            ]); ?>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary', 'name' => 'signup-button']); ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
