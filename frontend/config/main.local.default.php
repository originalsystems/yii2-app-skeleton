<?php
/**
 * Local frontend configuration file
 * (copy this to file w/o ".default" to use)
 */

return [
    'components' => [
        'request' => [
            'cookieValidationKey' => '',
        ],
    ],
];
