<?php

namespace yii\helpers;

class StringHelper extends BaseStringHelper
{
    public static function oct2string($oct)
    {
        $out = '';

        if( preg_match_all('#\d+#', $oct, $matches) ) {
            foreach( $matches[0] as $match ) {
                $out .= chr(octdec($match));
            }
        }

        return $out;
    }
}
