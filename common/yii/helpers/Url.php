<?php

namespace yii\helpers;

use Yii;

class Url extends BaseUrl
{
    /**
     * @param string|array $url
     * @param bool         $scheme
     *
     * @return string
     * @throws \yii\base\InvalidParamException
     */
    public static function to($url = '', $scheme = false)
    {
        if( is_array($url)
            && array_key_exists('_lang', $_GET)
            && array_key_exists('_lang', $url) === false
            && count(Yii::$app->getI18n()->languages) > 1
        ) {
            $url['_lang'] = Yii::$app->language;
        }

        return parent::to($url, $scheme);
    }
}
