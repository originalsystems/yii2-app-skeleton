<?php

namespace yii\helpers;

use Yii;
use common\web\Application;
use common\models\ar\Image;

class Html extends BaseHtml
{
    /**
     * Returns translated options for grid boolean filter or something else
     * @return array
     */
    public static function boolLabels()
    {
        return [
            0 => Yii::t('yii', 'No'),
            1 => Yii::t('yii', 'Yes'),
        ];
    }

    /**
     * Check for link equals current uri.
     * If so, add rel=nofollow to A tag.
     *
     * @param string $text
     * @param null   $url
     * @param array  $options
     *
     * @return string
     */
    public static function a($text, $url = NULL, $options = [])
    {
        if( Yii::$app instanceof Application ) {
            if( $url !== NULL ) {
                $url = $options['href'] = Url::to($url);
            }

            if( Yii::$app->getRequest()->getUrl() === $url ) {
                $options['rel'] = 'nofollow';
            }

            return static::tag('a', $text, $options);
        } else {
            return parent::a($text, $url, $options);
        }
    }

    public static $navLinkDefaultClass = 'btn btn-default tooltips';
    public static $navLinkCurrentClass = 'btn btn-primary tooltips';

    public static function navLink($label, $url = NULL, $title = NULL)
    {
        if( $url !== NULL ) {
            $url = Url::to($url);
        }

        $opts = [
            'class'      => self::$navLinkDefaultClass,
            'title'      => $title,
            'aria-label' => $title,
        ];

        if( strncmp($url, '/', 1) === 0 && strncmp($url, '//', 2) !== 0 ) {
            $controller = Yii::$app->controller;
            $parts      = explode('/', trim($url, '/'));
            $aID        = array_pop($parts);
            $cID        = array_pop($parts);

            if( $cID === $controller->id && $aID === $controller->action->id ) {
                $opts['class'] = self::$navLinkCurrentClass;
            }
        }

        return self::a($label, $url, $opts);
    }

    /**
     * @param \common\models\ar\Image $image
     * @param int                     $width
     * @param array                   $options
     *
     * @return string
     */
    public static function thumbPreview(Image $image = NULL, $width = 160, array $options = [])
    {
        if( $image === NULL ) {
            return NULL;
        }

        \common\assets\ThumbPreviewAsset::register(Yii::$app->view);

        self::addCssClass($options, 'fancybox-button');

        return self::a(self::img($image->getThumbUrl($width)), $image->path, $options);
    }

    /**
     * @param \common\models\ar\Image|NULL $image
     * @param int                          $width
     * @param array                        $options
     *
     * @return null|string
     */
    public static function imageHint(Image $image = NULL, $width = 160, array $options = [])
    {
        if( $image === NULL ) {
            return NULL;
        }

        $a = Html::thumbPreview($image, $width, $options);

        return '<p>' . Yii::t('app', 'Click on preview for detailed view.') . '</p>' . $a;
    }
}
