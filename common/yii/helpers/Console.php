<?php

namespace yii\helpers;

use Yii;
use yii\console\Exception;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use common\db\TableHelper;

class Console extends BaseConsole
{
    /**
     * @param string   $title
     * @param callable $callback
     * @param array    $params
     *
     * @return mixed
     */
    public static function timeout($title, $callback, $params = [])
    {
        $start = microtime(true);

        echo " > start {$title}..." . PHP_EOL;

        $result = call_user_func_array($callback, $params);
        $time   = sprintf('%.3f', microtime(true) - $start) . ' seconds';

        if( Yii::$app->controller->isColorEnabled() ) {
            $time = Console::ansiFormat($time, [Console::FG_GREEN]);
        }

        echo " > done {$title} in {$time}" . PHP_EOL;

        return $result;
    }

    public static function upStatTriggers()
    {
        $table = TableHelper::TABLE_STAT;
        $sql   = <<<SQL
CREATE TABLE {$table} (
    "schema_name" VARCHAR(255)  NOT NULL,
    "table_name"  VARCHAR(255)  NOT NULL,
    "updated_at"  TIMESTAMPTZ   DEFAULT CURRENT_TIMESTAMP,
    "row_count"   INT8 NOT NULL DEFAULT 0,
    PRIMARY KEY ("schema_name", "table_name")
);
SQL;

        Yii::$app->getDb()->createCommand($sql)->execute();

        $sql = <<<SQL
CREATE FUNCTION table_stat_insert() RETURNS TRIGGER AS
$$
BEGIN
    UPDATE {$table}
    SET "updated_at" = CURRENT_TIMESTAMP, "row_count" = "row_count" + 1
    WHERE "schema_name" = TG_TABLE_SCHEMA AND "table_name" = TG_TABLE_NAME;

    RETURN NEW;
END
$$ LANGUAGE plpgsql;
SQL;
        Yii::$app->getDb()->createCommand($sql)->execute();

        $sql = <<<SQL
CREATE FUNCTION table_stat_update() RETURNS TRIGGER AS
$$
BEGIN
    UPDATE {$table}
    SET "updated_at" = CURRENT_TIMESTAMP
    WHERE "schema_name" = TG_TABLE_SCHEMA AND "table_name" = TG_TABLE_NAME;

    RETURN NEW;
END
$$ LANGUAGE plpgsql;
SQL;
        Yii::$app->getDb()->createCommand($sql)->execute();

        $sql = <<<SQL
CREATE FUNCTION table_stat_delete() RETURNS TRIGGER AS
$$
BEGIN
    UPDATE {$table}
    SET "updated_at" = CURRENT_TIMESTAMP, "row_count" = "row_count" - 1
    WHERE "schema_name" = TG_TABLE_SCHEMA AND "table_name" = TG_TABLE_NAME;

    RETURN NEW;
END
$$ LANGUAGE plpgsql;
SQL;
        Yii::$app->getDb()->createCommand($sql)->execute();

        $sql = <<<SQL
CREATE FUNCTION table_stat_truncate() RETURNS TRIGGER AS
$$
BEGIN
    UPDATE {$table}
    SET "updated_at" = CURRENT_TIMESTAMP, "row_count" = 0
    WHERE "schema_name" = TG_TABLE_SCHEMA AND "table_name" = TG_TABLE_NAME;

    RETURN OLD;
END
$$ LANGUAGE plpgsql;
SQL;
        Yii::$app->getDb()->createCommand($sql)->execute();
    }

    public static function downStatTriggers()
    {
        Yii::$app->getDb()->createCommand('DROP FUNCTION table_stat_insert() CASCADE')->execute();
        Yii::$app->getDb()->createCommand('DROP FUNCTION table_stat_update() CASCADE')->execute();
        Yii::$app->getDb()->createCommand('DROP FUNCTION table_stat_delete() CASCADE')->execute();
        Yii::$app->getDb()->createCommand('DROP FUNCTION table_stat_truncate() CASCADE')->execute();
        Yii::$app->getDb()->createCommand('DROP TABLE "public"."table_stat"')->execute();
    }

    public static function createStatTriggers(array $tables = [])
    {
        if( count($tables) === 0 ) {
            return;
        }

        foreach( $tables as $table ) {
            if( list($schemaName, $tableName) = TableHelper::tableParts($table) ) {
                foreach( ['INSERT', 'UPDATE', 'DELETE', 'TRUNCATE'] AS $operation ) {
                    $opLC = strtolower($operation);
                    switch( $operation ) {
                        case 'INSERT':
                            $sql = <<<SQL
CREATE TRIGGER table_stat_on_{$schemaName}_{$tableName}_{$opLC} AFTER {$operation} ON {$table}
FOR EACH ROW EXECUTE PROCEDURE table_stat_insert()
SQL;
                            Yii::$app->getDb()->createCommand($sql)->execute();
                            break;
                        case 'UPDATE':
                            $sql = <<<SQL
CREATE TRIGGER table_stat_on_{$schemaName}_{$tableName}_{$opLC} AFTER {$operation} ON {$table}
FOR EACH ROW EXECUTE PROCEDURE table_stat_update()
SQL;
                            Yii::$app->getDb()->createCommand($sql)->execute();
                            break;
                        case 'DELETE':
                            $sql = <<<SQL
CREATE TRIGGER table_stat_on_{$schemaName}_{$tableName}_{$opLC} AFTER {$operation} ON {$table}
FOR EACH ROW EXECUTE PROCEDURE table_stat_delete()
SQL;
                            Yii::$app->getDb()->createCommand($sql)->execute();
                            break;

                        case 'TRUNCATE':
                            $sql = <<<SQL
CREATE TRIGGER table_stat_on_{$schemaName}_{$tableName}_{$opLC} AFTER {$operation} ON {$table}
EXECUTE PROCEDURE table_stat_truncate()
SQL;
                            Yii::$app->getDb()->createCommand($sql)->execute();
                            break;
                    }
                }
            }
        }

        $rows = array_filter(array_map([TableHelper::class, 'tableParts'], $tables));

        Yii::$app->getDb()->createCommand()->batchInsert(TableHelper::TABLE_STAT, ['schema_name', 'table_name'], $rows)->execute();
    }

    public static function dropStatTriggers(array $tables = [])
    {
        if( count($tables) === 0 ) {
            return;
        }

        foreach( $tables as $table ) {
            if( list($schemaName, $tableName) = TableHelper::tableParts($table) ) {
                foreach( ['INSERT', 'UPDATE', 'DELETE', 'TRUNCATE'] AS $operation ) {
                    $opLC = strtolower($operation);
                    switch( $operation ) {
                        case 'INSERT':
                            $sql = <<<SQL
DROP TRIGGER IF EXISTS table_stat_on_{$schemaName}_{$tableName}_{$opLC} ON {$table}
SQL;
                            Yii::$app->getDb()->createCommand($sql)->execute();
                            break;
                        case 'UPDATE':
                            $sql = <<<SQL
DROP TRIGGER IF EXISTS table_stat_on_{$schemaName}_{$tableName}_{$opLC} ON {$table}
SQL;
                            Yii::$app->getDb()->createCommand($sql)->execute();
                            break;
                        case 'DELETE':
                            $sql = <<<SQL
DROP TRIGGER IF EXISTS table_stat_on_{$schemaName}_{$tableName}_{$opLC} ON {$table}
SQL;
                            Yii::$app->getDb()->createCommand($sql)->execute();
                            break;

                        case 'TRUNCATE':
                            $sql = <<<SQL
DROP TRIGGER IF EXISTS table_stat_on_{$schemaName}_{$tableName}_{$opLC} ON {$table}
SQL;
                            Yii::$app->getDb()->createCommand($sql)->execute();
                            break;
                    }
                }
            }
        }

        $rows      = array_filter(array_map([TableHelper::class, 'tableParts'], $tables));
        $condition = ['OR'];

        foreach( $rows as list( $schemaName, $tableName ) ) {
            $condition[] = [
                'schema_name' => $schemaName,
                'table_name'  => $tableName,
            ];
        }

        Yii::$app->getDb()->createCommand()->delete(TableHelper::TABLE_STAT, $condition)->execute();
    }

    private static $_identities = [];

    /**
     * @param array $collection
     *
     * @throws Exception
     */
    public static function build($collection)
    {
        /**
         * @var \common\db\ActiveRecord $model
         */

        static $identities;

        if( $identities === NULL ) {
            $identities = [];
        }

        if( isset($collection['data']) === false ) {
            throw new Exception('Collection should contain class and data fields');
        }

        $table = isset($collection['table']) ? $collection['table'] : NULL;
        $class = isset($collection['class']) ? $collection['class'] : NULL;
        $data  = $collection['data'];

        if( is_array($data) === false ) {
            throw new Exception('Collection data should be array');
        }

        if( $class !== NULL ) {
            self::buildClassObject($class, $data);
        } elseif( $table !== NULL ) {
            self::buildTableRows($table, $data);
        } else {
            throw new Exception('Class or table should be specified');
        }
    }

    private static function buildClassObject($class, array $data)
    {
        /**
         * @var \common\db\ActiveRecord $model
         */

        if( class_exists($class) === false ) {
            throw new Exception('Collection class is not exists');
        }

        $class = new $class();

        if( $class instanceof ActiveRecord === false ) {
            throw new Exception('Collection class should extend \yii\db\ActiveRecord');
        }

        foreach( $data as $identity => $attributes ) {
            $meta  = isset($attributes['@meta']) ? $attributes['@meta'] : [];
            $model = clone $class;

            unset($attributes['@meta']);

            $attributes = self::prepareValues($attributes);

            foreach( $attributes as $name => $value ) {
                $model->{$name} = $value;
            }

            if( $model->save() === false ) {
                $errors  = $model->getFirstErrors();
                $message = 'Error during save model';
                if( count($errors) > 0 ) {
                    $message .= ': ' . current($errors);
                }
                throw new Exception($message);
            }

            if( is_numeric($identity) === false ) {
                self::$_identities['#' . $identity] = $model->getPrimaryKey();
            }

            self::buildAuth($model, $meta);
        }
    }

    private static function buildTableRows($table, array $data)
    {
        if( isset($data[0], $data[1]) === false ) {
            throw new Exception("Data for table \"{$table}\" should contain array of columns and array of values");
        }

        list($columns, $rawValues) = $data;

        $values = [];

        foreach( $rawValues as $value ) {
            $values[] = self::prepareValues($value);
        }

        Yii::$app->getDb()->createCommand()->batchInsert($table, $columns, $values)->execute();
    }

    private static function prepareValues(array $attributes)
    {
        foreach( $attributes as $attribute => &$value ) {
            if( is_string($value) && strpos($value, '#') === 0 && array_key_exists($value, self::$_identities) ) {
                $value = self::$_identities[$value];
            }
        }

        unset($value);

        return $attributes;
    }

    /**
     * @param ActiveRecord|IdentityInterface $model
     * @param array                          $meta
     */
    public static function buildAuth(ActiveRecord $model, $meta)
    {
        if( $model instanceof IdentityInterface === false ) {
            return;
        }

        if( isset($meta['roles']) === false || empty($meta['roles']) ) {
            return;
        }

        $auth  = Yii::$app->getAuthManager();
        $roles = (array)$meta['roles'];

        foreach( $roles as $roleName ) {
            $role = $auth->getRole($roleName);

            $auth->assign($role, $model->getId());
        }
    }
}
