<?php

namespace common\rbac;

use yii\helpers\ArrayHelper;
use yii\rbac\DbManager;

class AuthManager extends DbManager
{
    const ROLE_ADMIN = 'admin';

    protected $_assignments = [];

    protected function storeAssignments($userId)
    {
        if( array_key_exists($userId, $this->_assignments) === false ) {
            $this->_assignments[$userId] = ArrayHelper::index(parent::getAssignments($userId), 'roleName');
        }
    }

    public function getAssignments($userId)
    {
        $this->storeAssignments($userId);

        return $this->_assignments[$userId];
    }

    public function getAssignment($roleName, $userId)
    {
        $this->storeAssignments($userId);

        return isset($this->_assignments[$userId][$roleName]) ? $this->_assignments[$userId][$roleName] : NULL;
    }

    public static function roleLabels()
    {
        return [
            self::ROLE_ADMIN => \Yii::t('app', 'Administrator'),
        ];
    }
}
