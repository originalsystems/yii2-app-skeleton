<?php

namespace common\assets;

use common\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Class ThumbPreviewAsset
 * @package common\assets
 */
class ThumbPreviewAsset extends AssetBundle
{
    public $sourcePath = '@common/assets/thumb-preview';
    public $js         = [
        'thumb-preview.js',
    ];
    public $depends    = [
        JqueryAsset::class,
        FancyBoxAsset::class,
    ];
}
