<?php

namespace common\assets;

use yii\web\JqueryAsset;
use common\web\AssetBundle;

/**
 * Class CounterUpAsset, asset for bower-asset/jquery.counterup package
 * @package common\assets
 */
class CounterUpAsset extends AssetBundle
{
    public $sourcePath = '@bower/jquery.counterup';
    public $js         = [
        'jquery.counterup.min.js',
    ];

    public $depends = [
        JqueryAsset::class,
        WayPointAsset::class,
    ];

    public function init()
    {
        parent::init();

        $this->publishOptions['beforeCopy'] = function ($from, $to) {
            $dir  = \Yii::getAlias($this->sourcePath) . '/';
            $from = str_replace($dir, '', $from);

            return preg_match('#\.js$#', $from) === 1;
        };
    }
}
