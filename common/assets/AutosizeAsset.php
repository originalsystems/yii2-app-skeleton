<?php

namespace common\assets;

use common\web\AssetBundle;

/**
 * Class AutosizeAsset, asset for bower-asset/autosize package
 * @package common\assets
 */
class AutosizeAsset extends AssetBundle
{
    public $sourcePath = '@bower/autosize/dist';
    public $js         = [
        'autosize.min.js',
    ];
}
