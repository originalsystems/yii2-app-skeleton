jQuery(function () {
    "use strict";

    if (jQuery.fn.fancybox) {
        jQuery(".fancybox-button").fancybox({
            groupAttr:  'data-rel',
            prevEffect: 'none',
            nextEffect: 'none',
            closeBtn:   true,
            helpers:    {
                title: {
                    type: 'inside'
                }
            }
        });
    }
});
