<?php

namespace common\assets;

use common\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Class OwlCarouselAsset, asset for bower-asset/owl.carousel package
 * @package common\assets
 */
class OwlCarouselAsset extends AssetBundle
{
    public $sourcePath = '@bower/owl.carousel/dist';

    public $js  = [
        'owl.carousel.min.js',
    ];
    public $css = [
        'assets/owl.carousel.min.css',
    ];

    public $depends = [
        JqueryAsset::class,
    ];
}
