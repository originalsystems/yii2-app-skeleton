<?php

namespace common\assets;

use yii\web\JqueryAsset;
use common\web\AssetBundle;

/**
 * Class SlimScrollAsset, asset for bower-asset/jquery-slimscroll package
 * @package common\assets
 */
class SlimScrollAsset extends AssetBundle
{
    public $sourcePath = '@bower/jquery-slimscroll';
    public $js         = [
        'jquery.slimscroll.min.js',
    ];

    public $depends = [
        JqueryAsset::class,
    ];

    public function init()
    {
        parent::init();

        $this->publishOptions['beforeCopy'] = function ($from, $to) {
            $dir  = \Yii::getAlias($this->sourcePath) . '/';
            $from = str_replace($dir, '', $from);

            return preg_match('#^jquery\.slimscroll\.(min\.)?js$#', $from) === 1;
        };
    }
}
