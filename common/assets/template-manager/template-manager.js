/**
 * @type {{
 *      select_options: Function,
 *      modal: Function,
 *      tabbed_modal: Function,
 *      table: Function,
 *      pagination: Function,
 *      build_pagination: Function,
 * }}
 */
var TemplateManager = {};

jQuery(function () {

    "use strict";

    /**
     * @require {array} data, containing element like {value: string, label: string, data?:{[string]:[string]}, class?: string}
     */
    TemplateManager.select_options = _.template('<% _.each(data, function(el){ %><option value="<%- el.value %>"<% if (el.class) { %> class="<%- el.class %>"<% } if (el.data) { _.each(el.data, function(v, k){%> data-<%- k %>="<%- v %>"<%}); } %>><%- el.label %></option><% }); %>');

    /**
     * @type {function({title: string, body: string, modal_class?: string, buttons?: [{class: string, label: string, type: string, attributes: {[string]}}]}): string}
     */
    TemplateManager.modal = _.template('<div class="<%- data.modal_class || \'modal-dialog\' %>"><div class="modal-content"><div class="modal-header"><h4 class="modal-title text-center"><%= data.title %><button class="close" data-dismiss="modal">&times;</button></h4></div><div class="modal-body"><%= data.body %></div><% if (_.isEmpty(data.buttons) === false) { %><div class="modal-footer"><% _.each(data.buttons, function(button){ %><button class="<%- button.class || \'btn btn-default\' %>" type="<%- button.type || \'button\' %>" <% if (_.isEmpty(button.attributes) === false) { _.each(button.attributes, function(value, key){if (["class", "type"].indexOf(key) === -1) {print(_.escape(key) + \'="\' + _.escape(value) + \'" \');}}) } %>><%- button.label %></button><% }); %></div><% } %></div></div>', {variable: 'data'});

    /**
     * @require {array} tabs, containing elements like {title: string, body: string}
     */
    TemplateManager.tabbed_modal = _.template('<div class="<%- data.modal_class || \'modal-dialog\' %>"><div class="modal-content"><div class="portlet-body"><div class="modal-header"><ul class="nav nav-pills"><% _.chain(data.tabs).pluck(\'title\').each(function(title, index){ %><li<%= index === 0 ? \' class="active"\': \'\'%>><a data-toggle="tab" href="#tabbed_modal_<%= index %>"><%= title %></a></li><% }) %><button class="close" data-dismiss="modal">&times;</button></ul></div><div class="modal-body"><div class="tab-content"><% _.chain(data.tabs).pluck(\'body\').each(function(body, index){ %><div id="tabbed_modal_<%= index %>" class="tab-pane fade<%= index === 0 ? \' active in\': \'\'%>"><%= body %></div><% }) %></div></div></div></div></div>', {variable: 'data'});

    /**
     * @require {function({rows: Array, headers:Array, table_class?: string, empty_table_text: string, empty_cell_text: string}): string}
     */
    TemplateManager.table = _.template('<table class="<%- data.table_class || \'table table-bordered text-center\' %>"><% if (_.isEmpty(data.rows)) { %><thead><tr><th><%- data.empty_table_text || \'-- Нет данных --\' %></th></tr></thead><% } else { %><thead><tr><% _.each(data.headers, function(header){ %><th><%- header %></th><% }); %></tr></thead><tbody><% _.each(data.rows, function(row){ %><tr><% _.each(row, function(element){ %><td><% if (_.isEmpty(element)){ %><span class="text-muted"><%- data.empty_text_cell || \'-- Нет данных --\' %></span><% } else if (/^https?:\\/\\//.test(element)) { %><a href="<%- element %>" target="_blank"><%- element %></a><% } else { %> <%= element %> <% } %></td><% }); %></tr><% }); %></tbody><% } %></table>', {variable: 'data'});

    /**
     * @require {function({links: Array<{css_class: string, url: number, label: number}>)}
     */
    TemplateManager.pagination = _.template('<% if (_.isEmpty(links) === false) { %><ul class="pagination"><% _.each(links, function(link){ %><li class="<%- link.css_class %>"><a href="<%- link.url %>"><%- link.label %></a></li><% }); %></ul><% } %>');

    TemplateManager.build_pagination = function (url, current, count, length) {
        if (!length || length < 3) {
            length = 5;
        } else if (length % 2 == 0) {
            length += 1;
        }

        if (count <= 1) {
            return [];
        }

        if (current < 1) {
            current = 1;
        } else if (current > count) {
            current = count;
        }

        var hlf   = (length - 1) / 2,
            links = [],
            _url  = url.replace(/([?&])page=[^&]/, '$1').replace(/([?&])&+/, '$1').replace(/[?&]$/, '');

        _url += _url.indexOf('?') === -1 ? '?page=' : '&page=';

        function el(label, num) {
            return {
                css_class: num == current ? 'active' : '',
                url:       _url + num,
                label:     label
            };
        }

        var s, e;

        if (count <= length) {
            s = 1;
            e = count;
        } else {
            s = (current - hlf) < 1 ? 1 : (current - hlf);

            if (s > count - length + 1) {
                s = count - length + 1;
            }

            e = (current + hlf) > count ? count : (s + length - 1);
        }

        for (var i = s; i <= e; ++i) {
            links.push(el(i, i));
        }

        if (count > length) {
            if (current > hlf + 1) {
                links.unshift(el('<', 1));
            }

            if (current < count - hlf) {
                links.push(el('>', count));
            }
        }

        return TemplateManager.pagination({links: links});
    };

    jQuery('SCRIPT[type="underscore/template"][data-alias]').each(function () {
        TemplateManager[jQuery(this).data('alias')] = _.template(jQuery(this).html());
    });
});
