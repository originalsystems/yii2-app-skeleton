<?php

namespace common\assets;

use yii\web\JqueryAsset;
use common\web\AssetBundle;

/**
 * Class UniformAsset, asset for bower-asset/uniform package
 * @package common\assets
 */
class UniformAsset extends AssetBundle
{
    public $sourcePath = '@bower/uniform/dist';

    public $css = [
        'css/default.css',
    ];
    public $js  = [
        'js/jquery.uniform.standalone.js',
    ];

    public $depends = [
        JqueryAsset::class,
    ];
}
