<?php

namespace common\assets;

use common\web\AssetBundle;
use yii\web\JqueryAsset;

class SocketPushReceiverAsset extends AssetBundle
{
    public $sourcePath = '@common/assets/socket-push-receiver';
    public $js         = [
        'push-receiver.js',
    ];
    public $depends    = [
        JqueryAsset::class,
        SocketIOAsset::class,
    ];
}
