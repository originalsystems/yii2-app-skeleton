<?php

namespace common\assets;

use common\web\AssetBundle;

/**
 * Class BlockUIAsset, asset for bower-asset/blockui package
 * @package common\assets
 */
class BlockUIAsset extends AssetBundle
{
    public $sourcePath = '@bower/blockui';

    public $js = [
        'jquery.blockUI.js',
    ];
}
