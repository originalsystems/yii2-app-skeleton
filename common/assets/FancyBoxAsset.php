<?php

namespace common\assets;

use common\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Class FancyBoxAsset, asset for bower-asset/fancybox package
 * @package common\assets
 */
class FancyBoxAsset extends AssetBundle
{
    public $sourcePath = '@bower/fancybox/source';

    public $js  = [
        'jquery.fancybox.pack.js',
        'helpers/jquery.fancybox-buttons.js',
    ];
    public $css = [
        'jquery.fancybox.css',
        'helpers/jquery.fancybox-buttons.css',
    ];

    public $depends = [
        JqueryAsset::class,
    ];
}
