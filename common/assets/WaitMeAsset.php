<?php

namespace common\assets;

use yii\web\JqueryAsset;
use common\web\AssetBundle;

/**
 * Class WaitMeAsset
 * @package common\assets
 */
class WaitMeAsset extends AssetBundle
{
    public $sourcePath = '@common/assets/wait-me';

    public $js  = [
        'waitMe.min.js',
    ];
    public $css = [
        'waitMe.min.css',
    ];

    public $depends = [
        JqueryAsset::class,
    ];
}
