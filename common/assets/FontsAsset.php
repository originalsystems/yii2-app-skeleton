<?php

namespace common\assets;

use common\web\AssetBundle;

class FontsAsset extends AssetBundle
{
    public $sourcePath = '@common/assets/fonts';
    public $css        = [
        'fonts.css',
    ];
}
