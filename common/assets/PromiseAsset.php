<?php

namespace common\assets;

use common\web\AssetBundle;

/**
 * Class PromiseAsset, asset for bower-asset/es6-promise package
 * @package common\assets
 */
class PromiseAsset extends AssetBundle
{
    public $sourcePath = '@bower/es6-promise';

    public $js = [
        'es6-promise.auto.min.js',
    ];
}
