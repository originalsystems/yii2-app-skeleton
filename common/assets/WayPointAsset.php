<?php

namespace common\assets;

use common\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Class WayPointAsset, asset for bower-asset/waypoints package
 * @package common\assets
 */
class WayPointAsset extends AssetBundle
{
    public $sourcePath = '@bower/waypoints/lib';

    public $js = [
        'jquery.waypoints.min.js',
    ];

    public $depends = [
        JqueryAsset::class,
    ];
}
