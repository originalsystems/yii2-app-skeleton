<?php

namespace common\assets;

use common\web\AssetBundle;
use yii\web\JqueryAsset;

class TemplateManagerAsset extends AssetBundle
{
    public $sourcePath = '@common/assets/template-manager';
    public $js         = [
        'template-manager.js',
    ];

    public $depends = [
        JqueryAsset::class,
        UnderscoreAsset::class,
    ];
}
