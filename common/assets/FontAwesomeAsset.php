<?php

namespace common\assets;

use common\web\AssetBundle;

/**
 * Class FontAwesomeAsset, asset for bower-asset/fontawesome package
 * @package common\assets
 */
class FontAwesomeAsset extends AssetBundle
{
    public $sourcePath = '@bower/fontawesome';

    public $css = [
        'css/font-awesome.min.css',
    ];

    public function init()
    {
        parent::init();

        $this->publishOptions['beforeCopy'] = function ($from, $to) {
            return preg_match('%(/|\\\\)(fonts|css)%', $from);
        };
    }
}
