<?php

namespace common\assets;

use common\web\AssetBundle;

/**
 * Class SimpleLineIconsAsset, asset for bower-asset/simple-line-icons package
 * @package common\assets
 */
class SimpleLineIconsAsset extends AssetBundle
{
    public $sourcePath = '@bower/simple-line-icons';

    public $css = [
        'css/simple-line-icons.css',
    ];

    public function init()
    {
        parent:: init();

        $this->publishOptions['beforeCopy'] = function ($from, $to) {
            $dir  = \Yii::getAlias($this->sourcePath) . '/';
            $from = str_replace($dir, '', $from);

            return preg_match('#^(css|fonts)#', $from) === 1;
        };
    }
}
