<?php

namespace common\assets;

use common\web\AssetBundle;

/**
 * Class UnderscoreAsset, asset for bower-asset/underscore package
 * @package common\assets
 */
class UnderscoreAsset extends AssetBundle
{
    public $sourcePath = '@bower/underscore';
    public $js         = [
        'underscore-min.js',
    ];
}
