<?php

namespace common\assets;

use yii\web\JqueryAsset;
use yii\bootstrap\BootstrapPluginAsset;
use common\web\AssetBundle;

class MetronicAsset extends AssetBundle
{
    public $js  = [
        '/public/metronic/js/app.js',
        '/public/metronic/js/layout.js',
    ];
    public $css = [
        '/public/metronic/components.css',
        '/public/metronic/plugins.css',
        '/public/metronic/layout.css',

        /**
         * Color themes
         */
        '/public/metronic/themes/default.css',
        //'/public/metronic/themes/blue.css',
        //'/public/metronic/themes/dark.css',
        //'/public/metronic/themes/grey.css',
        //'/public/metronic/themes/light.css',
    ];

    public $depends = [
        JqueryAsset::class,
        BootstrapPluginAsset::class,
        FontsAsset::class,
        FontAwesomeAsset::class,
        UnderscoreAsset::class,
        SweetAlertAsset::class,
        SimpleLineIconsAsset::class,
        UniformAsset::class,
        BlockUIAsset::class,
        CookieAsset::class,
        SlimScrollAsset::class,
    ];
}
