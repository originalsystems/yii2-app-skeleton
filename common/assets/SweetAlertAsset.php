<?php

namespace common\assets;

use common\web\AssetBundle;

/**
 * Class SweetAlertAsset, asset for bower-asset/sweetalert2 package
 * @package common\assets
 */
class SweetAlertAsset extends AssetBundle
{
    public $sourcePath = '@bower/sweetalert2/dist';

    public $js  = [
        'sweetalert2.min.js',
    ];
    public $css = [
        'sweetalert2.min.css',
    ];

    public $depends = [
        PromiseAsset::class,
    ];
}
