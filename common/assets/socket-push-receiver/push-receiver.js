jQuery(function () {
    "use strict";

    var socket_url = null,
        user_hash  = null;

    if (socket_url === null && user_hash === null) {
        console.warn("specify socket url and user hash at first");
        return;
    }

    var socket = io(socket_url);

    socket.emit('login', user_hash);

    socket.on('reconnect', function (num) {
        socket.emit('login', user_hash);
    });

    socket.on('message', function (message) {
        jQuery(window).trigger(message.event, [message.data]);
    });
});
