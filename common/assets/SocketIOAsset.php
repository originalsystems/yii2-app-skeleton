<?php

namespace common\assets;

use common\web\AssetBundle;

class SocketIOAsset extends AssetBundle
{
    public $sourcePath = '@common/assets/socket-io';
    public $js         = [
        'socket.io.js',
    ];
}
