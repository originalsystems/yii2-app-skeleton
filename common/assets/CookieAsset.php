<?php

namespace common\assets;

use yii\web\JqueryAsset;
use common\web\AssetBundle;

/**
 * Class CookieAsset, asset for bower-asset/jquery-cookie package
 * @package common\assets
 */
class CookieAsset extends AssetBundle
{
    public $sourcePath = '@bower/jquery-cookie';

    public $js = [
        'jquery.cookie.js',
    ];

    public $depends = [
        JqueryAsset::class,
    ];
}
