<?php

namespace common\behaviors;

use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\helpers\FileHelper;

class DeleteFilesBehavior extends Behavior
{
    public $files;
    public $fileGetter;
    public $deleteEmptyDirectories = true;

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_DELETE => 'deleteFiles',
        ];
    }

    public function deleteFiles()
    {
        $files = (array)$this->files;

        if( is_callable($this->fileGetter) ) {
            $files = array_merge($files, (array)call_user_func($this->fileGetter));
        }

        foreach( $files as $file ) {
            @unlink($file);

            if( $this->deleteEmptyDirectories === true ) {
                $dir   = dirname($file);
                $files = FileHelper::findFiles($dir, ['recursive' => false]);

                while( count($files) === 0 ) {
                    rmdir($dir);
                    $dir   = dirname($dir);
                    $files = FileHelper::findFiles($dir, ['recursive' => false]);
                }
            }
        }
    }
}
