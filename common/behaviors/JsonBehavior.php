<?php

namespace common\behaviors;

use yii\base\Behavior;
use yii\base\Event;
use yii\base\InvalidConfigException;
use yii\db\BaseActiveRecord;

/**
 * Class ExtraParamBehavior
 * @package common\behaviors
 */
class JsonBehavior extends Behavior
{
    /**
     * Map of json attributes, that should be decoded after find and encoded before save
     *   - field => model-property
     * @var array
     */
    public $map = [
        'extra_param' => 'json',
    ];

    /**
     * @param \yii\base\Component $owner
     *
     * @throws \yii\base\InvalidConfigException
     */
    public function attach($owner)
    {
        parent::attach($owner);

        $properties = array_values($this->map);

        if( count(array_count_values($properties)) !== count($properties) ) {
            throw new InvalidConfigException(__CLASS__ . ': map can not contain duplicate values.');
        }

        foreach( $properties as $property ) {
            if( $owner->hasProperty($property) === false ) {
                $className = get_class($owner);

                throw new InvalidConfigException(__CLASS__ . ": class {$className} should contain property \${$property}");
            }
        }
    }

    /**
     * @return array
     */
    public function events()
    {
        return [
            BaseActiveRecord::EVENT_AFTER_FIND    => 'readJson',
            BaseActiveRecord::EVENT_BEFORE_UPDATE => 'writeJson',
            BaseActiveRecord::EVENT_BEFORE_INSERT => 'writeJson',
        ];
    }

    /**
     * Decoding json field after find
     *
     * @param Event $event
     */
    public function readJson(Event $event)
    {
        foreach( $this->map as $field => $property ) {
            $this->owner->{$property} = json_decode($this->owner->{$field}, true) ? : [];
        }
    }

    /**
     * Setting up json encode string in active record on insert or update
     *
     * @param Event $event
     */
    public function writeJson(Event $event)
    {
        foreach( $this->map as $field => $property ) {
            $json = $this->owner->{$property};

            if( is_array($json) === false ) {
                $json = [];
            }

            $this->owner->{$field} = json_encode($json);
        }
    }
}
