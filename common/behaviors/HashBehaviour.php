<?php

namespace common\behaviors;

use common\base\Security;
use yii\behaviors\AttributeBehavior;
use yii\db\BaseActiveRecord;

/**
 * Class UniqueUUIDBehaviour
 * @package common\behaviors
 * @property \yii\db\BaseActiveRecord $owner
 */
class HashBehaviour extends AttributeBehavior
{
    const TYPE_SHORT = 'short';
    const TYPE_LONG  = 'long';

    public $is_unique  = true;
    public $hash_type  = self::TYPE_SHORT;
    public $attributes = [
        BaseActiveRecord::EVENT_BEFORE_VALIDATE => 'unique_hash',
        BaseActiveRecord::EVENT_BEFORE_UPDATE   => 'unique_hash',
        BaseActiveRecord::EVENT_BEFORE_INSERT   => 'unique_hash',
    ];

    /**
     * @param \yii\base\Event $event
     *
     * @return string
     */
    protected function getValue($event)
    {
        $owner = $this->owner;
        $query = $owner::find();

        if( $owner->getAttribute('unique_hash') !== NULL ) {
            return $owner->getAttribute('unique_hash');
        }

        $hash = $this->generateHash($this->hash_type);

        if( $this->is_unique === true ) {
            while( $query->where(['unique_hash' => $hash])->exists() ) {
                $hash = $this->generateHash($this->hash_type);
            }
        }

        return $hash;
    }

    /**
     * Generates hash of taken type
     *
     * @param $type
     *
     * @return null|string
     */
    protected function generateHash($type)
    {
        switch( $type ) {
            case self::TYPE_LONG:

                return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
                    mt_rand(0, 0xffff), mt_rand(0, 0xffff),
                    mt_rand(0, 0xffff),
                    mt_rand(0, 0x0fff) | 0x4000,
                    mt_rand(0, 0x3fff) | 0x8000,
                    mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
                );
            case self::TYPE_SHORT:

                return sprintf('%04x%04x', mt_rand(0, 0xffff), mt_rand(0, 0xffff));

            default:
                return NULL;
        }
    }
}
