<?php

namespace common\behaviors;

use yii\db\BaseActiveRecord;
use yii\db\Expression;

/**
 * Class TimestampBehavior with check existing of attributes and strict value and attributes
 * @package common\behaviors
 * @property BaseActiveRecord $owner
 */
class TimestampBehavior extends \yii\behaviors\TimestampBehavior
{
    /**
     * @param BaseActiveRecord $owner
     */
    public function attach($owner)
    {
        $this->attributes[BaseActiveRecord::EVENT_BEFORE_INSERT] = [];
        $this->attributes[BaseActiveRecord::EVENT_BEFORE_UPDATE] = [];

        if( $owner->hasAttribute($this->createdAtAttribute) ) {
            $this->attributes[BaseActiveRecord::EVENT_BEFORE_INSERT][] = $this->createdAtAttribute;
        }

        if( $owner->hasAttribute($this->updatedAtAttribute) ) {
            $this->attributes[BaseActiveRecord::EVENT_BEFORE_INSERT][] = $this->updatedAtAttribute;
            $this->attributes[BaseActiveRecord::EVENT_BEFORE_UPDATE][] = $this->updatedAtAttribute;
        }

        parent::attach($owner);
    }

    protected function getValue($event)
    {
        return new Expression('NOW()');
    }
}
