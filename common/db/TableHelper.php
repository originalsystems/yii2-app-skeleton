<?php

namespace common\db;

class TableHelper
{
    const TABLE_STAT        = '"public"."table_stat"';
    const TABLE_CLEAR_REGEX = '/[{}%"\']/';

    public static function clearTableName($table)
    {
        return preg_replace(self::TABLE_CLEAR_REGEX, '', $table);
    }

    public static function tableParts($table)
    {
        $pair = explode('.', self::clearTableName($table));

        if( count($pair) === 1 ) {
            return ['public', $pair[0]];
        } elseif( count($pair) === 2 ) {
            return [$pair[0], $pair[1]];
        }

        return NULL;
    }
}
