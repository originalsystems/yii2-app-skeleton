<?php

namespace common\db\pgsql;

use yii\base\InvalidParamException;

class QueryBuilder extends \yii\db\pgsql\QueryBuilder
{
    protected $conditionBuilders = [
        'NOT'                      => 'buildNotCondition',
        'AND'                      => 'buildAndCondition',
        'OR'                       => 'buildAndCondition',
        'BETWEEN'                  => 'buildBetweenCondition',
        'NOT BETWEEN'              => 'buildBetweenCondition',
        'IN'                       => 'buildInCondition',
        'NOT IN'                   => 'buildInCondition',
        'LIKE'                     => 'buildLikeCondition',
        'ILIKE'                    => 'buildLikeCondition',
        'NOT LIKE'                 => 'buildLikeCondition',
        'NOT ILIKE'                => 'buildLikeCondition',
        'OR LIKE'                  => 'buildLikeCondition',
        'OR ILIKE'                 => 'buildLikeCondition',
        'OR NOT LIKE'              => 'buildLikeCondition',
        'OR NOT ILIKE'             => 'buildLikeCondition',
        'EXISTS'                   => 'buildExistsCondition',
        'NOT EXISTS'               => 'buildExistsCondition',
        'JSONB_EXISTS'             => 'buildJsonExistsCondition',
        'JSONB_CONTAINS'           => 'buildJsonContainsCondition',
        'JSONB_EXTRACT_PATH_TEXT'  => 'buildJsonExtractPathTextCondition',
        'JSONB_ARRAY_ELEMENT'      => 'buildJsonSimpleCondition',
        'JSONB_OBJECT_FIELD'       => 'buildJsonSimpleCondition',
        'JSONB_ARRAY_ELEMENT_TEXT' => 'buildJsonSimpleCondition',
        'JSONB_OBJECT_FIELD_TEXT'  => 'buildJsonSimpleCondition',
    ];

    public function buildJsonExistsCondition($operator, $operands, &$params)
    {
        return $this->buildJsonSimpleCondition($operator, $operands, $params);
    }

    public function buildJsonContainsCondition($operator, $operands, &$params)
    {
        if( count($operands) !== 2 ) {
            throw new InvalidParamException("Operator '$operator' requires two operands.");
        }

        list($column, $condition) = $operands;

        if( is_array($condition) ) {
            $encode = json_encode($condition);
        } elseif( is_string($condition) ) {
            $encode = '"' . addslashes($condition) . '"';
        } else {
            $encode = $condition;
        }

        return "{$operator} ({$column}, '{$encode}')";
    }

    public function buildJsonExtractPathTextCondition($operator, $operands, &$params)
    {
        if( count($operands) !== 3 ) {
            throw new InvalidParamException("Operator '$operator' requires three operands.");
        }

        list($column, $dottedPath, $value) = $operands;

        $dottedPath     = array_map(function ($value) {
            return "'{$value}'";
        }, explode('.', $dottedPath));
        $pathStringArgs = implode(', ', $dottedPath);

        $phName          = self::PARAM_PREFIX . count($params);
        $params[$phName] = $value;

        return "$operator ($column, $pathStringArgs) = $phName";
    }

    public function buildJsonSimpleCondition($operator, $operands, &$params)
    {
        if( count($operands) !== 2 ) {
            throw new InvalidParamException("Operator '$operator' requires two operands.");
        }

        list($column, $value) = $operands;

        $phName          = self::PARAM_PREFIX . count($params);
        $params[$phName] = $value;

        return "$operator ($column,  $phName)";
    }
}
