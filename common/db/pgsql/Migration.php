<?php

namespace common\db\pgsql;

use yii\helpers\Console;

class Migration extends \yii\db\Migration
{
    public $schemas            = [];
    public $tables             = [];
    public $views              = [];
    public $materialized_views = [];
    public $indexes            = [];

    public function safeUp()
    {
        $class      = static::class;
        $migration  = preg_replace('/^m\d+_\d+_/', '', $class);
        $reflection = new \ReflectionClass($class);
        $map        = [
            'upSchemas' => $migration . ' up schemas',
            'upTables'  => $migration . ' up tables',
            'upViews'   => $migration . ' up views',
            'upIndexes' => $migration . ' up indexes',
            'upExtra'   => $migration . ' up extra',
        ];

        foreach( $map as $method => $description ) {
            /**
             * method "upSchemas" has default behavior, unlike others
             */
            if( $method === 'upSchemas' && !empty($this->schemas) ) {
                Console::timeout($description, [$this, $method]);

                continue;
            }

            if( $reflection->getMethod($method)->getDeclaringClass()->name === $class ) {
                Console::timeout($description, [$this, $method]);
            }
        }
    }

    public function safeDown()
    {
        $this->downExtra();
        $this->downIndexes();
        $this->downViews();
        $this->downTables();
        $this->downSchemas();
    }

    /**
     * Realize method in migration class
     */
    public function upSchemas()
    {
        foreach( $this->schemas as $schema ) {
            $this->execute("CREATE SCHEMA {$schema}");
        }
    }

    /**
     * Realize method in migration class
     */
    public function upTables()
    {
    }

    /**
     * Realize method in migration class
     */
    public function upViews()
    {
    }

    /**
     * Realize method in migration class
     */
    public function upIndexes()
    {
    }

    /**
     * Realize method in migration class
     */
    public function upExtra()
    {
    }

    /**
     * Realize method in migration class
     */
    public function downExtra()
    {
    }

    public function downIndexes()
    {
        foreach( array_reverse($this->indexes) as $index ) {
            $this->execute("DROP INDEX {$index};");
        }
    }

    public function downViews()
    {
        foreach( array_reverse($this->materialized_views) as $view ) {
            $this->execute("DROP MATERIALIZED VIEW {$view};");
        }

        foreach( array_reverse($this->views) as $view ) {
            $this->execute("DROP VIEW {$view};");
        }
    }

    public function downTables()
    {
        foreach( array_reverse($this->tables) as $table ) {
            $this->execute("DROP TABLE {$table};");
        }
    }

    public function downSchemas()
    {
        foreach( array_reverse($this->schemas) as $schema ) {
            $this->execute("DROP SCHEMA {$schema};");
        }
    }
}
