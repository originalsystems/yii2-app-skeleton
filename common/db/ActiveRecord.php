<?php

namespace common\db;

use Yii;
use yii\base\Exception;
use yii\caching\DbDependency;
use yii\web\NotFoundHttpException;
use common\mixin\ModelTrait;
use common\mixin\ExtraBehavior;

class ActiveRecord extends \yii\db\ActiveRecord
{
    use ExtraBehavior, ModelTrait;

    /**
     * Apply extra behaviors on specific scenario
     *
     * @param bool $runValidation
     * @param null $attributes
     *
     * @return bool
     * @throws \Exception
     * @throws \yii\base\InvalidParamException
     */
    public function insert($runValidation = true, $attributes = NULL)
    {
        $this->applyExtraBehaviors('insert');

        return parent::insert($runValidation, $attributes);
    }

    /**
     * Apply extra behaviors on specific scenario
     *
     * @param bool $runValidation
     * @param null $attributeNames
     *
     * @return false|int
     * @throws \Exception
     * @throws \yii\db\StaleObjectException
     * @throws \yii\base\InvalidParamException
     */
    public function update($runValidation = true, $attributeNames = NULL)
    {
        $this->applyExtraBehaviors('update');

        return parent::update($runValidation, $attributeNames);
    }

    /**
     * Apply extra behaviors on specific scenario
     *
     * @return false|int
     * @throws \Exception
     * @throws \yii\base\InvalidParamException
     * @throws \yii\db\StaleObjectException
     */
    public function delete()
    {
        $this->applyExtraBehaviors('delete');

        return parent::delete();
    }

    /**
     * Apply extra behaviors on specific scenario
     *
     * @return bool
     * @throws \yii\base\InvalidParamException
     */
    public function beforeValidate()
    {
        $scenario = $this->getIsNewRecord() === true ? 'insert' : 'update';
        $this->applyExtraBehaviors($scenario);

        return parent::beforeValidate();
    }

    /**
     * Build database dependency for current table and tables in parameter, based on table "table_stat".
     * Current table contain information about last table update and row count. You can use that method, if
     * table has a lot of hooks, writing via \yii\helpers\Console::createStatTriggers method
     *
     * @param array $tables
     *
     * @return \yii\caching\DbDependency
     */
    public static function dependency(array $tables = [])
    {
        $tables    = (array)$tables;
        $tables[]  = static::tableName();
        $condition = implode(' OR ', array_map(function ($table) {
            list($schemaName, $tableName) = TableHelper::tableParts($table);

            return <<<SQL
("schema_name" = '{$schemaName}' AND "table_name" = '{$tableName}')
SQL;
        }, $tables));

        $sql = <<<SQL
SELECT MAX("updated_at") FROM "table_stat" WHERE {$condition};
SQL;

        return new DbDependency(['sql' => $sql]);
    }

    /**
     * @param string    $message
     * @param bool|true $runValidation
     * @param null      $attributeNames
     *
     * @return bool
     * @throws \yii\base\Exception
     */
    public function strictSave($message = NULL, $runValidation = true, $attributeNames = NULL)
    {
        $result = parent::save($runValidation, $attributeNames);

        if( $message === NULL ) {
            $message = Yii::t('app', 'Record cannot be saved');
        }

        if( $result === false ) {
            $errors = $this->getFirstErrors();

            if( count($errors) > 0 ) {
                $message .= ', ' . current($errors);
            }

            throw new Exception($message);
        }

        return $result;
    }

    /**
     * @param mixed  $condition
     * @param string $message
     *
     * @return static
     * @throws \yii\web\NotFoundHttpException
     */
    public static function needOne($condition, $message = NULL)
    {
        /**
         * @var ActiveRecord $result
         */
        $result = parent::findOne($condition);

        if( $message === NULL ) {
            $message = Yii::t('app', 'The requested page does not exist.');
        }

        if( $result === NULL ) {
            throw new NotFoundHttpException(Yii::t('app', $message));
        }

        return $result;
    }

    /**
     * Returns array of columns for current model table in format
     * - "table_name"."column_name_1"
     * - "table_name"."column_name_2"
     *
     * @param string $alias
     *
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public static function tableColumns($alias = NULL)
    {
        if( $alias === NULL ) {
            list($schema, $alias) = TableHelper::tableParts(static::tableName());
        }

        $columns = array_keys(static::getTableSchema()->columns);

        return array_map(function ($column) use ($alias) {
            return "\"{$alias}\".\"{$column}\"";
        }, $columns);
    }
}
