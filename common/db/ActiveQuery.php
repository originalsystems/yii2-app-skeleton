<?php

namespace common\db;

use Yii;
use yii\web\NotFoundHttpException;

class ActiveQuery extends \yii\db\ActiveQuery
{
    /**
     * @param null   $db
     *
     * @param string $message
     *
     * @return array|null|\yii\db\ActiveRecord
     * @throws \yii\web\NotFoundHttpException
     */
    public function needOne($db = NULL, $message = 'The requested page does not exist.')
    {
        $model = $this->one($db);

        if( $model === NULL ) {
            throw new NotFoundHttpException(Yii::t('app', $message));
        }

        return $model;
    }
}
