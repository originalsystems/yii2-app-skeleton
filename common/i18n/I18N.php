<?php

namespace common\i18n;

use Yii;
use yii\base\InvalidConfigException;
use yii\db\Query;
use yii\i18n\DbMessageSource;
use yii\i18n\MessageSource;
use yii\i18n\MissingTranslationEvent;

/**
 * Class I18N
 * @package common\i18n
 */
class I18N extends \yii\i18n\I18N
{
    /**
     * @type array
     */
    public $languages = [
        'en',
    ];

    /**
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\NotSupportedException
     * @throws \yii\db\Exception
     */
    public function init()
    {
        parent::init();

        if( is_array($this->languages) === false || count($this->languages) === 0 ) {
            throw new InvalidConfigException(__CLASS__ . ': $languages should be non-empty array');
        }

        if( in_array(Yii::$app->language, $this->languages, true) === false ) {
            throw new InvalidConfigException(__CLASS__ . ': Application language should be in array of available languages');
        }

        $this->initDbStorage();
    }

    /**
     * @throws \yii\db\Exception
     * @throws \yii\base\NotSupportedException
     * @throws \yii\base\InvalidConfigException
     */
    private function initDbStorage()
    {
        $event         = MessageSource::EVENT_MISSING_TRANSLATION;
        $messageSource = $this->getMessageSource('app');

        $messageSource->on($event, function (MissingTranslationEvent $event) {
            $this->addTranslation(Yii::$app->language, $event->message, $event->message, $event->category);
        });
    }

    /**
     * @param string $language
     * @param string $source
     * @param string $translation
     * @param string $category
     *
     * @throws \yii\db\Exception
     * @throws \yii\base\NotSupportedException
     * @throws \yii\base\InvalidConfigException
     */
    public function addTranslation($language, $source, $translation, $category = 'app')
    {
        /**
         * @var DbMessageSource $messageSource
         */
        $messageSource = Yii::$app->getI18n()->getMessageSource($category);
        $db            = Yii::$app->getDb();
        $params        = [
            'category' => $category,
            'message'  => $source,
        ];

        $query = new Query();
        $query->select(['id']);
        $query->from($messageSource->sourceMessageTable);
        $query->where($params);

        $id = $query->scalar();

        if( $id === false ) {
            $data = $db->getSchema()->insert($messageSource->sourceMessageTable, [
                'category' => $category,
                'message'  => $source,
            ]);

            $id = (int)$data['id'];
        }

        $db->createCommand()->delete($messageSource->messageTable, [
            'id'       => $id,
            'language' => $language,
        ])->execute();

        $db->createCommand()->insert($messageSource->messageTable, [
            'id'          => $id,
            'language'    => $language,
            'translation' => $translation,
        ])->execute();

        foreach( $this->languages as $anotherLanguage ) {
            if( $language !== $anotherLanguage ) {
                $query = new Query();

                $query->from($messageSource->messageTable);
                $query->andWhere([
                    'id'       => $id,
                    'language' => $anotherLanguage,
                ]);

                if( $query->exists() ) {
                    continue;
                }

                $db->createCommand()->insert($messageSource->messageTable, [
                    'id'          => $id,
                    'language'    => $anotherLanguage,
                    'translation' => $source,
                ])->execute();
            }
        }
    }

    public function usedCategories()
    {
        $sql = <<<SQL
SELECT DISTINCT "category"
FROM "message_source";
SQL;

        return Yii::$app->getDb()->createCommand($sql)->queryColumn();
    }
}
