<?php

namespace common\mixin;

use yii\helpers\ArrayHelper;
use yii\base\Behavior;

/**
 * Class ExtraBehavior
 * @package common\base\mixin
 * @method Behavior   getBehavior($name)
 * @method Behavior[] getBehaviors()
 * @method Behavior   attachBehavior($name, $behavior)
 * @method void       attachBehaviors($behaviors)
 * @method void       ensureBehaviors()
 */
trait ExtraBehavior
{
    private $appliedScenarios = [];

    /**
     * @return array
     */
    public function extraBehaviors()
    {
        return [];
    }

    /**
     * @param string $scenario
     *
     * @return $this
     * @throws \yii\base\InvalidParamException
     */
    public function applyExtraBehaviors($scenario)
    {
        $this->ensureBehaviors();

        if( !in_array($scenario, $this->appliedScenarios, true) ) {
            $this->attachBehaviors(ArrayHelper::getValue($this->extraBehaviors(), $scenario, []));

            $this->appliedScenarios[] = $scenario;
        }

        return $this;
    }
}
