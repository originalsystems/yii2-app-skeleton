<?php

namespace common\mixin;

use Yii;

/**
 * Class ModelFlashTrait
 * @package common\mixin
 * @method array getFirstErrors()
 */
trait ModelTrait
{
    /**
     * @return array
     */
    public function attributeLabels()
    {
        return static::labels();
    }

    /**
     * @return array
     */
    public static function labels()
    {
        return [];
    }

    /**
     * @param string $message
     */
    public function flashSuccess($message = NULL)
    {
        $message = $message ? : Yii::t('app', 'Transmitted information has been saved successfully.');

        Yii::$app->getSession()->setFlash('success', $message);
    }

    /**
     * @param string $message
     */
    public function flashError($message = NULL)
    {
        $message = $message ? : Yii::t('app', 'An unexpected error occurred while data processing');
        $errors  = $this->getFirstErrors();
        $message = current($errors) ? : $message;

        Yii::$app->getSession()->setFlash('error', $message);
    }
}
