<?php

namespace common\widgets\assets;

use common\web\AssetBundle;

class LanguageSwitcherAsset extends AssetBundle
{
    public $sourcePath = '@common/widgets/assets/language-switcher';
    public $css        = [
        'style.css',
    ];
}
