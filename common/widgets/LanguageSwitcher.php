<?php

namespace common\widgets;

use Yii;
use yii\base\Widget;
use yii\bootstrap\Nav;
use yii\helpers\Html;
use common\widgets\assets\LanguageSwitcherAsset;

class LanguageSwitcher extends Widget
{
    public $wrapperTag     = 'li';
    public $wrapperOptions = [
        'class' => 'language-switcher-controls',
    ];

    public function run()
    {
        $items = $this->listItems();

        if( count($items) <= 1 ) {
            return '';
        }

        LanguageSwitcherAsset::register(Yii::$app->getView());

        $widget = Nav::widget([
            'items'        => $items,
            'encodeLabels' => false,
            'options'      => [
                'class' => [
                    'widget' => false,
                ],
            ],
        ]);

        if( $this->wrapperTag !== NULL ) {
            $widget = Html::tag($this->wrapperTag, $widget, $this->wrapperOptions);
        }

        return $widget;
    }

    protected function listItems()
    {
        $items = [];

        foreach( Yii::$app->getI18n()->languages as $language ) {
            $items[] = [
                'label'   => "<img src=\"/public/image/flags/{$language}.png\">",
                'url'     => [NULL, '_lang' => $language] + $_GET,
                'active'  => Yii::$app->language === $language,
                'options' => [
                    'class' => 'lang',
                ],
            ];
        }

        return $items;
    }
}
