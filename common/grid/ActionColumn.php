<?php

namespace common\grid;

use Yii;
use yii\helpers\Html;

class ActionColumn extends \yii\grid\ActionColumn
{
    public $buttonDefaultCssClass = 'btn btn-default tooltips';

    public $template = '<div class="btn-group btn-group-sm">{view}{update}{delete}</div>';

    /**
     * Initializes the default button rendering callbacks.
     */
    protected function initDefaultButtons()
    {
        if( !isset($this->buttons['view']) ) {
            $this->buttons['view'] = function ($url, $model, $key) {
                $options = array_merge([
                    'title'          => Yii::t('yii', 'View'),
                    'aria-label'     => Yii::t('yii', 'View'),
                    'class'          => $this->buttonDefaultCssClass,
                    'data-pjax'      => '0',
                    'data-placement' => 'bottom',
                ], $this->buttonOptions);

                return Html::a('<i class="fa fa-eye"></i>', $url, $options);
            };
        }
        if( !isset($this->buttons['update']) ) {
            $this->buttons['update'] = function ($url, $model, $key) {
                $options = array_merge([
                    'title'          => Yii::t('yii', 'Update'),
                    'aria-label'     => Yii::t('yii', 'Update'),
                    'class'          => $this->buttonDefaultCssClass,
                    'data-pjax'      => '0',
                    'data-placement' => 'bottom',
                ], $this->buttonOptions);

                return Html::a('<i class="fa fa-pencil"></i>', $url, $options);
            };
        }
        if( !isset($this->buttons['delete']) ) {
            $this->buttons['delete'] = function ($url, $model, $key) {
                $options = array_merge([
                    'title'          => Yii::t('yii', 'Delete'),
                    'aria-label'     => Yii::t('yii', 'Delete'),
                    'class'          => str_replace('btn-default', 'btn-danger', $this->buttonDefaultCssClass),
                    'data-confirm'   => Yii::t('yii', 'Are you sure you want to delete this item?'),
                    'data-method'    => 'post',
                    'data-pjax'      => '0',
                    'data-placement' => 'bottom',
                ], $this->buttonOptions);

                return Html::a('<i class="fa fa-trash"></i>', $url, $options);
            };
        }
    }
}
