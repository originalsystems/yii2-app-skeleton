<?php

namespace common\rest;

use Yii;
use yii\base\ErrorException;
use yii\base\Exception;
use yii\base\UserException;
use yii\web\HttpException;
use yii\web\Response;

class ErrorHandler extends \yii\web\ErrorHandler
{
    /**
     * @param \Exception $exception
     *
     * @throws \yii\base\InvalidParamException
     */
    protected function renderException($exception)
    {
        if( Yii::$app->has('response') ) {
            $response = Yii::$app->getResponse();
            // reset parameters of response to avoid interference with partially created response data
            // in case the error occurred while sending the response.
            $response->isSent  = false;
            $response->stream  = NULL;
            $response->data    = NULL;
            $response->content = NULL;
        } else {
            $response = new Response();
        }

        if( in_array($response->format, [Response::FORMAT_JSON, Response::FORMAT_XML], true) ) {
            $response->data = $this->convertExceptionToArray($exception);

            if( $exception instanceof HttpException ) {
                $response->setStatusCode($exception->statusCode);
            } else {
                $response->setStatusCode(500);
            }
            $response->send();

            return;
        }

        parent::renderException($exception);
    }

    /**
     * @param \Exception $ex
     *
     * @return array
     */
    public function convertExceptionToArray($ex)
    {
        $code = $ex->getCode() ? : -1;
        $data = [
            'code' => $code,
            'data' => [
                'error' => 'Error',
            ],
        ];

        if( $ex instanceof UserException || YII_DEBUG === true ) {
            $data['data']['error']   = ($ex instanceof Exception || $ex instanceof ErrorException) ? $ex->getName() : 'Exception';
            $data['data']['message'] = $ex->getMessage();
        }

        if( YII_DEBUG ) {
            $data['exception']   = get_class($ex);
            $data['file']        = $ex->getFile();
            $data['line']        = $ex->getLine();
            $data['stack-trace'] = explode("\n", $ex->getTraceAsString());
            if( $ex instanceof \yii\db\Exception ) {
                $data['error-info'] = $ex->errorInfo;
            }

            if( ($prev = $ex->getPrevious()) !== NULL ) {
                $data['previous'] = $this->convertExceptionToArray($prev);
            }
        }

        ksort($data);

        return $data;
    }
}
