<?php

namespace common\rest;

use yii\base\Model;
use yii\base\InvalidConfigException;
use yii\web\ForbiddenHttpException;

class ActiveController extends Controller
{
    /**
     * @var string the model class name. This property must be set.
     */
    public $modelClass;
    /**
     * @var string the scenario used for updating a model.
     * @see \yii\base\Model::scenarios()
     */
    public $updateScenario = Model::SCENARIO_DEFAULT;
    /**
     * @var string the scenario used for creating a model.
     * @see \yii\base\Model::scenarios()
     */
    public $createScenario = Model::SCENARIO_DEFAULT;

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        if( $this->modelClass === NULL ) {
            throw new InvalidConfigException('The "modelClass" property must be set.');
        }
    }

    protected $allowedActions = [
        'index',
        'view',
        'create',
        'update',
        'delete',
        'options',
    ];

    public function actions()
    {
        $actions = [];

        if( in_array('index', $this->allowedActions, true) ) {
            $actions['index'] = [
                'class'       => 'yii\rest\IndexAction',
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ];
        }

        if( in_array('view', $this->allowedActions, true) ) {
            $actions['view'] = [
                'class'       => 'yii\rest\ViewAction',
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ];
        }

        if( in_array('create', $this->allowedActions, true) ) {
            $actions['create'] = [
                'class'       => 'yii\rest\CreateAction',
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'scenario'    => $this->createScenario,
            ];
        }

        if( in_array('update', $this->allowedActions, true) ) {
            $actions['update'] = [
                'class'       => 'yii\rest\UpdateAction',
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'scenario'    => $this->updateScenario,
            ];
        }

        if( in_array('delete', $this->allowedActions, true) ) {
            $actions['delete'] = [
                'class'       => 'yii\rest\DeleteAction',
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ];
        }

        if( in_array('options', $this->allowedActions, true) ) {
            $actions['options'] = [
                'class' => 'yii\rest\OptionsAction',
            ];
        }

        return $actions;
    }

    /**
     * @inheritdoc
     */
    protected function verbs()
    {
        return [
            'index'  => ['GET', 'HEAD'],
            'view'   => ['GET', 'HEAD'],
            'create' => ['POST'],
            'update' => ['PUT', 'PATCH'],
            'delete' => ['DELETE'],
        ];
    }

    /**
     * Checks the privilege of the current user.
     *
     * This method should be overridden to check whether the current user has the privilege
     * to run the specified action against the specified data model.
     * If the user does not have access, a [[ForbiddenHttpException]] should be thrown.
     *
     * @param string $action the ID of the action to be executed
     * @param object $model  the model to be accessed. If null, it means no specific model is being accessed.
     * @param array  $params additional parameters
     *
     * @throws ForbiddenHttpException if the user does not have access
     */
    public function checkAccess($action, $model = NULL, $params = [])
    {
    }
}
