<?php

namespace common\rest;

use yii\filters\ContentNegotiator;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\RateLimiter;
use yii\filters\VerbFilter;
use common\web\Response;

abstract class Controller extends \yii\rest\Controller
{
    public    $enableCsrfValidation = false;
    public    $serializer           = [
        'class'              => 'common\rest\Serializer',
        'collectionEnvelope' => 'data',
    ];
    protected $authenticate         = true;
    protected $authenticateOptional = [];
    protected $wrapResponse         = true;

    public function behaviors()
    {
        $behaviors = [
            'contentNegotiator' => [
                'class'   => ContentNegotiator::class,
                'formats' => [
                    'application/json'       => Response::FORMAT_JSON,
                    'application/javascript' => Response::FORMAT_JSON,
                    'application/xml'        => Response::FORMAT_XML,
                    'application/xml+dtd'    => Response::FORMAT_XML,
                    'application/atom+xml'   => Response::FORMAT_XML,
                    'application/soap+xml'   => Response::FORMAT_XML,
                    '*/*'                    => Response::FORMAT_JSON,
                ],
            ],
            'verbFilter'        => [
                'class'   => VerbFilter::class,
                'actions' => $this->verbs(),
            ],
            'rateLimiter'       => [
                'class' => RateLimiter::class,
            ],
        ];

        if( $this->authenticate === true ) {
            $behaviors['authenticator'] = [
                'class'      => QueryParamAuth::class,
                'tokenParam' => 'access_token',
                'optional'   => $this->authenticateOptional,
            ];
        }

        $accessRules = $this->accessRules();

        if( count($accessRules) !== 0 ) {
            $behaviors['access'] = [
                'class' => AccessControl::class,
                'rules' => $accessRules,
            ];
        }

        return $behaviors;
    }

    /**
     * Return access rules array for common\filters\AccessControl behavior
     * @see common\rest\Controller::behaviors()
     * @see common\filters\AccessControl
     * @return array
     */
    public function accessRules()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function afterAction($action, $result)
    {
        $data = parent::afterAction($action, $result);

        if( $this->wrapResponse === true && (is_scalar($data) || is_array($data)) ) {
            /**
             * Not wrapping in case of exist 'data' key
             */
            if( is_array($data) && isset($data['data']) ) {
                $data['code'] = 0;
            } else {
                $data = [
                    'code' => 0,
                    'data' => $data,
                ];
            }

            ksort($data);
        }

        return $data;
    }
}
