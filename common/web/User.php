<?php

namespace common\web;

/**
 * Class User
 * @package common\web
 * @method \common\models\ar\User getIdentity($autoRenew = true)
 * @property \common\models\ar\User $identity
 */
class User extends \yii\web\User
{
}
