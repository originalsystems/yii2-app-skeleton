<?php

namespace common\web;

/**
 * Class Request
 * @package  common\web
 * @property bool        $isMobile
 * @property bool        $isDesktop
 * @property string|null $subDomain
 * @property string|null $frontendBaseUrl
 * @property string|null $backendBaseUrl
 * @property string|null $apiBaseUrl
 */
class Request extends \yii\web\Request
{
    protected $_content_type;

    /**
     * @return string
     */
    public function getContentType()
    {
        if( $this->_content_type === NULL ) {
            $this->_content_type = parent::getContentType();
        }

        return $this->_content_type;
    }

    /**
     * @param mixed $value
     */
    public function setContentType($value)
    {
        $this->_content_type = $value;
    }

    /**
     * @var bool|array
     */
    private $_env = false;

    /**
     * Check a lot of params about current environment, calling once per request
     */
    private function ensureEnvironment()
    {
        if( $this->_env !== false ) {
            return;
        }

        /**
         * Defaults
         */
        $this->_env = [
            'is-mobile'    => false,
            'sub-domain'   => NULL,
            'frontend-url' => NULL,
            'backend-url'  => NULL,
            'api-url'      => NULL,
        ];

        /**
         * Check device type, at first try use nginx flags IS_MOBILE and IS_DESKTOP
         */
        if( isset($_SERVER['IS_MOBILE'], $_SERVER['IS_DESKTOP']) ) {
            $this->_env['is-mobile'] = (bool)$_SERVER['IS_MOBILE'];
        } elseif( !empty($_SERVER['HTTP_USER_AGENT']) ) {
            if( preg_match('#((android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge\ |maemo|midp|mmp|netfront|opera\ m(ob|in)i|palm(\ os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows\ (ce|phone)|xda|xiino)#i', $_SERVER['HTTP_USER_AGENT']) === 1 ) {
                $this->_env['is-mobile'] = true;
            } elseif( preg_match('#^(1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a\ wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r\ |s\ )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1\ u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp(\ i|ip)|hs\-c|ht(c(\-|\ |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac(\ |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt(\ |\/)|klon|kpt\ |kwc\-|kyo(c|k)|le(no|xi)|lg(\ g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-|\ |o|v)|zz)|mt(50|p1|v\ )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v\ )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-|\ )|webc|whit|wi(g\ |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-)#', $_SERVER['HTTP_USER_AGENT']) === 1 ) {
                $this->_env['is-mobile'] = true;
            }
        }

        /**
         * Check sub-domain, in case of it is using. For example:
         *
         * example.org              => NULL
         * example.user.in          => NULL
         * test.example.org         => "test"
         * test.example.user.in     => "test"
         * pay.test.example.org     => "pay.test"
         * pay.test.example.user.in => "pay.test"
         */
        $host_parts       = explode('.', $_SERVER['HTTP_HOST']);
        $sub_domain_parts = [];
        $is_local         = $host_parts[count($host_parts) - 1] === 'in';
        $limit            = $is_local ? 3 : 2;

        while( count($host_parts) > $limit ) {
            $sub_domain_parts[] = array_shift($host_parts);
        }

        if( count($sub_domain_parts) > 0 ) {
            $this->_env['sub-domain'] = implode('.', $sub_domain_parts);
        }

        /**
         * Calculate frontend, backend and api base urls for current environment
         */
        $host = $this->getHostInfo();

        if( $host !== NULL ) {
            $this->_env['frontend-url'] = preg_replace('#://(api|admin)\.#', '://', $host);
            $this->_env['backend-url']  = preg_replace('#://#', '://admin.', $this->_env['frontend-url']);
            $this->_env['api-url']      = preg_replace('#://#', '://api.', $this->_env['frontend-url']);
        }
    }

    /**
     * @return bool
     */
    public function getIsMobile()
    {
        $this->ensureEnvironment();

        return $this->_env['is-mobile'];
    }

    /**
     * @return bool
     */
    public function getIsDesktop()
    {
        $this->ensureEnvironment();

        return $this->_env['is-mobile'] === false;
    }

    /**
     * @return null|string
     */
    public function getSubDomain()
    {
        $this->ensureEnvironment();

        return $this->_env['sub-domain'];
    }

    /**
     * @return string|string
     */
    public function getFrontendBaseUrl()
    {
        $this->ensureEnvironment();

        return $this->_env['frontend-url'];
    }

    /**
     * @return string|string
     */
    public function getBackendBaseUrl()
    {
        $this->ensureEnvironment();

        return $this->_env['backend-url'];
    }

    /**
     * @return string|string
     */
    public function getApiBaseUrl()
    {
        $this->ensureEnvironment();

        return $this->_env['api-url'];
    }
}
