<?php

namespace common\web;

interface AuthControllerInterface
{
    public function actionSignIn();

    public function actionSignOut();
}
