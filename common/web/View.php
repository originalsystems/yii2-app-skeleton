<?php

namespace common\web;

use Yii;

/**
 * Class View
 * @package common\web
 * @property string $description
 * @property string $keywords
 */
class View extends \yii\web\View
{
    public $breadcrumbs = [];

    /**
     * @param string $url
     * @param array  $options
     * @param string $key
     *
     * @throws \yii\base\InvalidParamException
     */
    public function registerJsFile($url, $options = [], $key = NULL)
    {
        if( Yii::$app->getAssetManager()->appendTimestamp === true ) {
            $url = $this->appendTimestamp($url);
        }

        parent::registerJsFile($url, $options, $key);
    }

    /**
     * @param string $url
     * @param array  $options
     * @param string $key
     *
     * @throws \yii\base\InvalidParamException
     */
    public function registerCssFile($url, $options = [], $key = NULL)
    {
        if( Yii::$app->getAssetManager()->appendTimestamp === true ) {
            $url = $this->appendTimestamp($url);
        }

        parent::registerCssFile($url, $options, $key);
    }

    /**
     * @param string $file
     *
     * @return string
     * @throws \yii\base\InvalidParamException
     */
    public function appendTimestamp($file)
    {
        if( strpos($file, '/') === 0 ) {
            $path = Yii::getAlias('@webroot') . $file;

            if( file_exists($path) && ($timestamp = @filemtime($path)) > 0 ) {
                return $file . (strpos($file, '?') === false ? '?' : '&') . 'v=' . $timestamp;
            }
        }

        return $file;
    }

    /**
     * Try to append css and js files by path mask from @webroot, can used placeholders
     *
     * #ACTION_ID#     - action id
     * #CONTROLLER_ID# - controller id
     * #MODULE_ID#     - module id, if exists
     * #MODULE_PATH#   - path of chaining modules id, like "cabinet/statistic/daily"
     * #EXTENSION#     - file extension, trying to append, like "css" or "js"
     *
     * @param string $mask
     *
     * @throws \yii\base\InvalidParamException
     */
    public function registerRelatedFiles($mask)
    {
        $dir = Yii::getAlias('@webroot');

        $module       = Yii::$app->controller->module;
        $actionID     = Yii::$app->controller->action->id;
        $controllerID = Yii::$app->controller->id;
        $moduleID     = NULL;
        $moduleIDList = [];

        while( $module instanceof Application === false ) {
            $moduleID = $moduleIDList[] = $module->id;
            $module   = $module->module;
        }

        $modulePath = implode('/', $moduleIDList);

        $script = preg_replace('#/{2,}#', '/', strtr($mask, [
            '#ACTION_ID#'     => $actionID,
            '#CONTROLLER_ID#' => $controllerID,
            '#MODULE_ID#'     => $moduleID,
            '#MODULE_PATH#'   => $modulePath,
            '#EXTENSION#'     => 'js',
        ]));

        $file = $dir . $script;

        if( file_exists($file) ) {
            $this->registerJsFile($script, ['depends' => array_keys($this->assetBundles)]);
        }

        $style = preg_replace('#/{2,}#', '/', strtr($mask, [
            '#ACTION_ID#'     => $actionID,
            '#CONTROLLER_ID#' => $controllerID,
            '#MODULE_ID#'     => $moduleID,
            '#MODULE_PATH#'   => $modulePath,
            '#EXTENSION#'     => 'css',
        ]));
        $file  = $dir . $style;

        if( file_exists($file) ) {
            $this->registerCssFile($style, ['depends' => array_keys($this->assetBundles)]);
        }
    }

    /**
     * @var string|null
     */
    private $_description;

    /**
     * @return string|null
     */
    public function getDescription()
    {
        return $this->_description;
    }

    /**
     * @param string|null $description
     */
    public function setDescription($description)
    {
        $this->registerMetaTag(['name' => 'description', 'content' => $description], 'page-description');
        $this->_description = $description;
    }

    /**
     * @var string|null
     */
    private $_keywords;

    /**
     * @return string|null
     */
    public function getKeywords()
    {
        return $this->_keywords;
    }

    /**
     * @param string|array $keywords
     */
    public function setKeywords($keywords)
    {
        if( is_array($keywords) ) {
            $keywords = implode(', ', $keywords);
        }

        $this->registerMetaTag(['name' => 'keywords', 'content' => $keywords], 'page-keywords');
        $this->_keywords = $keywords;
    }
}
