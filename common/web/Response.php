<?php

namespace common\web;

use Yii;

class Response extends \yii\web\Response
{
    public function jsRedirect($url, $method = 'get', $data = [])
    {
        $this->format = self::FORMAT_HTML;
        $this->data   = Yii::$app->controller->renderPartial('@common/web/views/js-redirect', [
            'url'    => $url,
            'method' => $method,
            'data'   => $data,
        ]);
        Yii::$app->end();
    }
}
