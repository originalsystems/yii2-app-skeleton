<?php

namespace common\web;

use Yii;
use yii\base\Action;
use yii\base\InvalidConfigException;

/**
 * Class Application
 * @package common\web
 * @property \yii\redis\Cache         $cache
 * @property \yii\redis\Connection    $redis
 * @property \common\rbac\AuthManager $authManager
 * @property \common\web\AssetManager $assetManager
 * @property \common\web\Controller   $controller
 * @property \common\base\Curl        $curl
 * @property \common\i18n\I18N        $i18n
 * @property \common\web\User         $user
 * @property \common\web\View         $view
 * @property \common\web\Request      $request
 * @property \common\web\Response     $response
 *
 * @method \yii\redis\Cache         getCache()
 * @method \common\rbac\AuthManager getAuthManager()
 * @method \common\web\AssetManager getAssetManager()
 * @method \common\i18n\I18N        getI18n()
 * @method \common\web\User         getUser()
 * @method \common\web\View         getView()
 * @method \common\web\Request      getRequest()
 * @method \common\web\Response     getResponse()
 */
class Application extends \yii\web\Application
{
    public $enableSeoRedirects = false;

    /**
     * @param \yii\base\Action $action
     *
     * @return bool
     */
    public function beforeAction($action)
    {
        $this->languageSettingUp();
        $this->seoRedirect($action);

        return parent::beforeAction($action);
    }

    private function languageSettingUp()
    {
        $session = Yii::$app->getSession();
        $request = Yii::$app->getRequest();
        $i18n    = Yii::$app->getI18n();

        if( $this->language !== NULL && in_array($this->language, $i18n->languages, true) === false ) {
            throw new InvalidConfigException('Default language should be is an available language');
        }

        $isSingle = count($i18n->languages) === 1;
        $language = $request->get('_lang');

        /**
         * Get parameter _lang is primary flag
         */
        if( in_array($language, $i18n->languages, true) ) {
            /**
             * Remove lang parameter in single lang applications
             */
            if( $isSingle ) {
                Yii::$app->getResponse()->redirect([NULL, '_lang' => NULL]);
                Yii::$app->end();
            }
            $this->language = $language;
            $session->set('_lang', $language);

            return;
        }

        /**
         * Unsupported language, redirects to default language or empty language in single lang application
         * @TODO is this error?
         */
        if( $language !== NULL ) {
            $language = $isSingle ? NULL : $this->language;
            Yii::$app->getResponse()->redirect([NULL, '_lang' => $language]);
            Yii::$app->end();
        }

        /**
         * Returns in case of single language application
         */
        if( $isSingle ) {
            return;
        }

        /**
         * @TODO check header "Accept"
         */

        $language = $session->get('_lang');

        /**
         * Looks like unique click, try check from geo ip
         */
        if( $language === NULL && function_exists('geoip_country_code_by_name') ) {
            $code     = strtolower(geoip_country_code_by_name($request->getUserIP()));
            $language = in_array($code, $i18n->languages, true) ? $code : $this->language;
        }

        if( $language === NULL ) {
            $language = $this->language;
        }

        $session->set('_lang', $language);

        $this->language = $language;
    }

    private function seoRedirect(Action $action)
    {
        if ($this->enableSeoRedirects !== true) {
            return;
        }

        if( $action->id === 'error' ) {
            return;
        }

        $parts = explode('?', $_SERVER['REQUEST_URI']);
        $orig  = $path = $parts[0];

        if( $path === '/' ) {
            return;
        }

        $path = rtrim($path, '/');
        $path = preg_replace('#/index$#', '', $path);
        $path = preg_replace('#/site$#', '', $path);
        $path = preg_replace('#/default$#', '', $path);

        if( $path === '' ) {
            $path = '/';
        }

        if( $path !== $orig ) {
            $path .= isset($parts[1]) ? '?' . $parts[1] : NULL;

            Yii::$app->getResponse()->redirect($path);
            Yii::$app->end();
        }
    }
}
