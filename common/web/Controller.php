<?php

namespace common\web;

use Yii;
use yii\base\Action;
use yii\base\Model;
use yii\filters\AccessControl;
use yii\filters\AccessRule;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;

class Controller extends \yii\web\Controller
{
    public function init()
    {
        parent::init();

        $this->attachAuthAccessRules();
    }

    protected function ajaxValidation(Model $model)
    {
        if( Yii::$app->getRequest()->getIsAjax() && Yii::$app->getRequest()->getIsPjax() === false ) {
            $model->load(Yii::$app->getRequest()->post());
            $res         = Yii::$app->getResponse();
            $res->format = Response::FORMAT_JSON;
            $res->data   = ActiveForm::validate($model);
            Yii::$app->end(0, $res);
        }
    }

    protected function attachAuthAccessRules()
    {
        if( $this instanceof AuthControllerInterface ) {
            $this->attachBehaviors([
                'access' => [
                    'class' => AccessControl::class,
                    'only'  => ['sign-in', 'sign-out'],
                    'rules' => [
                        [
                            'actions'      => ['sign-in'],
                            'allow'        => true,
                            'roles'        => ['?'],
                            'denyCallback' => function (AccessRule $rule, Action $action) {
                                return $action->controller->goHome();
                            },
                        ],
                        [
                            'actions'      => ['sign-out'],
                            'allow'        => true,
                            'roles'        => ['@'],
                            'denyCallback' => function (AccessRule $rule, Action $action) {
                                return $action->controller->goHome();
                            },
                        ],
                    ],
                ],
                'verbs'  => [
                    'class'   => VerbFilter::class,
                    'actions' => [
                        'logout' => ['post'],
                    ],
                ],
            ]);
        }
    }
}
