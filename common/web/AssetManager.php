<?php

namespace common\web;

use Yii;
use yii\helpers\Html;

class AssetManager extends \yii\web\AssetManager
{
    /**
     * Compress all registered local js files into one minified file via node-uglifyjs
     * @var bool
     */
    public $compressJs = false;

    /**
     * Location of node-uglifyjs for compress local js files
     * @var string
     */
    public $uglifyJsLocation = '@node_modules/.bin/uglifyjs';

    /**
     * Compress all registered local css files into one minified file via node-cleancss
     * @var bool
     */
    public $compressCss = false;

    /**
     * Location of node-cleancss for compress local css files
     * @var string
     */
    public $cleanCssLocation = '@node_modules/.bin/cleancss';

    /**
     * @var int cache duration in seconds
     */
    public $cacheDuration = 300;

    public function init()
    {
        parent::init();

        $this->registerCompressFiles();
    }

    private function registerCompressFiles()
    {
        if( $this->compressCss !== true && $this->compressJs !== true ) {
            return;
        }

        Yii::$app->view->on(View::EVENT_END_PAGE, function () {
            $view = Yii::$app->view;

            if( $this->compressJs === true && !empty($view->jsFiles[View::POS_END]) ) {
                if( !empty($view->jsFiles[View::POS_HEAD]) ) {
                    $files = $view->jsFiles[View::POS_HEAD];

                    $view->jsFiles[View::POS_HEAD] = $this->registerCompressJsFiles($files);
                }

                if( !empty($view->jsFiles[View::POS_BEGIN]) ) {
                    $files = $view->jsFiles[View::POS_BEGIN];

                    $view->jsFiles[View::POS_BEGIN] = $this->registerCompressJsFiles($files);
                }

                if( !empty($view->jsFiles[View::POS_END]) ) {
                    $files = $view->jsFiles[View::POS_END];

                    $view->jsFiles[View::POS_END] = $this->registerCompressJsFiles($files);
                }
            }

            if( $this->compressCss === true && count($view->cssFiles) !== 0 ) {
                $files = $view->cssFiles;

                $view->cssFiles = $this->registerCompressCssFiles($files);
            }
        });
    }

    private function registerCompressJsFiles(array $files)
    {
        $web     = Yii::getAlias('@web');
        $webroot = Yii::getAlias('@webroot');
        $uglify  = Yii::getAlias($this->uglifyJsLocation);

        $localFiles = [];
        $result     = [];

        foreach( $files as $file => $tag ) {
            $clearFile = explode('?', $file)[0];
            $path      = $webroot . $clearFile;

            if( strpos($file, '/') === 0 && file_exists($path) && ($ts = filemtime($path)) > 0 ) {
                $localFiles[$clearFile] = $ts;
            } else {
                $result[$file] = $tag;
            }
        }

        if( count($localFiles) === 0 ) {
            return $result;
        }

        $name = md5(serialize($localFiles)) . '.js';
        $path = $webroot . '/assets/minify/' . $name;
        $url  = $web . '/assets/minify/' . $name;

        if( is_dir(dirname($path)) === false ) {
            mkdir(dirname($path));
        }

        if( file_exists($path) ) {
            $result[$url] = Html::jsFile($url);

            return $result;
        }

        $cache = "uglifyjs-state-{$name}";

        if( Yii::$app->getCache()->get($cache) === true ) {
            return $files;
        }

        Yii::$app->getCache()->set($cache, true, $this->cacheDuration);

        $fileList = implode(' ', array_map(function ($uri) {
            return '../..' . $uri;
        }, array_keys($localFiles)));
        $command  = /** @lang Bash */
            <<<BASH
cd `realpath {$webroot}/assets/minify`; 
node {$uglify} {$fileList} \
    --source-map=`realpath {$path}.map` \
    --source-map-url={$name}.map \
    -c -m -o=`realpath {$path}` 1>/dev/null 2>&1 &
BASH;

        exec($command);

        return $files;
    }

    private function registerCompressCssFiles(array $files)
    {
        $web      = Yii::getAlias('@web');
        $webroot  = Yii::getAlias('@webroot');
        $cleanCss = Yii::getAlias($this->cleanCssLocation);

        $localFiles = [];
        $result     = [];

        foreach( $files as $file => $tag ) {
            $clearFile = explode('?', $file)[0];
            $path      = $webroot . $clearFile;

            if( strpos($file, '/') === 0 && file_exists($path) && ($ts = filemtime($path)) > 0 ) {
                $localFiles[$clearFile] = $ts;
            } else {
                $result[$file] = $tag;
            }
        }

        if( count($localFiles) === 0 ) {
            return $result;
        }

        $name = md5(serialize($localFiles)) . '.css';
        $path = $webroot . '/assets/minify/' . $name;
        $url  = $web . '/assets/minify/' . $name;

        if( is_dir(dirname($path)) === false ) {
            mkdir(dirname($path));
        }

        if( file_exists($path) ) {
            $result[$url] = Html::cssFile($url);

            return $result;
        }

        $cache = "cleancss-state-{$name}";

        if( Yii::$app->getCache()->get($cache) === true ) {
            return $files;
        }

        Yii::$app->getCache()->set($cache, true, $this->cacheDuration);

        $fileList = implode(' ', array_map(function ($uri) {
            return '../..' . $uri;
        }, array_keys($localFiles)));

        $command = /** @lang Bash */
            <<<BASH
cd `realpath {$webroot}/assets/minify`; 
node {$cleanCss} \
    --source-map \
    --output `realpath {$path}` \
    {$fileList} 1>/dev/null 2>&1 &
BASH;
        exec($command);

        return $files;
    }
}
