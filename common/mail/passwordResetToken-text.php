<?php

/**
 * @var \common\web\View       $this
 * @var \common\models\ar\User $user
 */

$resetLink = Yii::$app->getUrlManager()->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
?>
Hello <?= $user->name ?>,

Follow the link below to reset your password:

<?= $resetLink ?>
