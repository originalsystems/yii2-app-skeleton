<?php
use yii\helpers\Html;

/**
 * @var \common\web\View       $this
 * @var \common\models\ar\User $user
 */

$resetLink = Yii::$app->getUrlManager()->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
?>
<div class="password-reset">
    <p>Hello <?= Html::encode($user->name) ?>,</p>

    <p>Follow the link below to reset your password:</p>

    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
</div>
