<?php

namespace common\console;

class Application extends \yii\console\Application
{
    public $availableLanguages = [];

    public function init()
    {
        if( $_SERVER['argc'] === 2 && $_SERVER['argv'][1] === 'data' ) {
            $_SERVER['argv'][1] = 'help';
            $_SERVER['argv'][2] = 'data';
        }

        parent::init();
    }
}
