<?php

namespace common\models\aq;

/**
 * This is the ActiveQuery class for [[\common\models\ar\Image]].
 *
 * @see \common\models\ar\Image
 * @method \common\models\ar\Image[]|array all($db = NULL)
 * @method \common\models\ar\Image|array|null one($db = NULL)
 */
class ImageQuery extends \common\db\ActiveQuery
{
}
