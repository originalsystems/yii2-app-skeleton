<?php

namespace common\models\aq;

/**
 * This is the ActiveQuery class for [[\common\models\ar\MessageTranslation]].
 *
 * @see \common\models\ar\MessageTranslation
 * @method \common\models\ar\MessageTranslation one($db = NULL)
 * @method \common\models\ar\MessageTranslation[] all($db = NULL)
 */
class MessageTranslationQuery extends \common\db\ActiveQuery
{
}
