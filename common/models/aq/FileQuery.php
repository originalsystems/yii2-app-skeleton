<?php

namespace common\models\aq;

use common\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\common\models\ar\File]].
 *
 * @see \common\models\ar\File
 * @method \common\models\ar\File[]|array all($db = NULL)
 * @method \common\models\ar\File|array|null one($db = NULL)
 */
class FileQuery extends ActiveQuery
{
}
