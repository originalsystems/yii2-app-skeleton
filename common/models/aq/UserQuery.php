<?php

namespace common\models\aq;

use common\db\ActiveQuery;

/**
 * Class UserQuery
 * @package common\models\aq
 * @method \common\models\ar\User[]|array all($db = NULL)
 * @method \common\models\ar\User|array|null one($db = NULL)
 */
class UserQuery extends ActiveQuery
{
}
