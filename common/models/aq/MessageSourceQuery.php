<?php

namespace common\models\aq;

/**
 * This is the ActiveQuery class for [[\common\models\ar\MessageSource]].
 *
 * @see \common\models\ar\MessageSource
 * @method \common\models\ar\MessageSource one($db = NULL)
 * @method \common\models\ar\MessageSource[] all($db = NULL)
 */
class MessageSourceQuery extends \common\db\ActiveQuery
{
}
