<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "message_source".
 *
 * @property integer              $id
 * @property string               $category
 * @property string               $message
 *
 * @property MessageTranslation[] $messageTranslations
 */
class MessageSource extends \common\db\ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return 'public.message_source';
    }

    /**
     * @return \common\models\aq\MessageSourceQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\aq\MessageSourceQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['message'], 'string'],
            [['category'], 'string', 'max' => 255],
        ];
    }

    public static function labels()
    {
        return [
            'id'       => Yii::t('app', 'ID'),
            'category' => Yii::t('app', 'Category'),
            'message'  => Yii::t('app', 'Message'),
        ];
    }

    /**
     * @return \common\models\aq\MessageTranslationQuery
     */
    public function getMessageTranslations()
    {
        return $this->hasMany(MessageTranslation::className(), ['id' => 'id'])->indexBy('language')->inverseOf('messageSource');
    }
}
