<?php

namespace common\models\ar;

use Yii;
use yii\base\InvalidCallException;
use yii\base\InvalidParamException;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;
use common\db\ActiveRecord;
use common\behaviors\DeleteFilesBehavior;
use common\behaviors\TimestampBehavior;
use common\models\aq\FileQuery;

/**
 * Class File
 * @package common\models\ar
 *
 * @property integer $id
 * @property string  $content_type
 * @property string  $original_name
 * @property string  $path
 * @property string  $created_at
 */
class File extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return 'public.file';
    }

    /**
     * @return FileQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FileQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function extraBehaviors()
    {
        return [
            'insert' => [
                TimestampBehavior::class,
            ],
            'delete' => [
                [
                    'class'      => DeleteFilesBehavior::class,
                    'fileGetter' => [$this, 'getRealPath'],
                ],
            ],
        ];
    }

    public function rules()
    {
        return [
            [['content_type', 'original_name', 'path'], 'required'],
            [['original_name'], 'string', 'max' => 256],
            [['created_at'], 'safe'],
            [['content_type'], 'string', 'max' => 128],
            [['path'], 'string', 'max' => 512],
        ];
    }

    public static function labels()
    {
        return [
            'id'            => Yii::t('app', 'ID'),
            'content_type'  => Yii::t('app', 'Content type'),
            'original_name' => Yii::t('app', 'Original name'),
            'path'          => Yii::t('app', 'Path to file'),
            'created_at'    => Yii::t('app', 'Created at'),
        ];
    }

    /**
     * @return string
     */
    public function getWebUrl()
    {
        return $this->path;
    }

    /**
     * @return string
     */
    public function getRealPath()
    {
        return APP_ROOT . $this->path;
    }

    /**
     * @param \yii\web\UploadedFile $file
     * @param bool                  $deleteTempFile
     * @param string                $className
     *
     * @return File
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\InvalidCallException
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidParamException
     */
    public static function upload(UploadedFile $file, $deleteTempFile = true, $className = File::class)
    {
        do {
            $name = md5(microtime() . mt_rand(0, PHP_INT_MAX)) . '.' . $file->getExtension();
            $hash = substr($name, 0, 3);
            $dir  = Yii::getAlias('@upload/' . $hash);
            $path = $dir . '/' . $name;
        } while( file_exists($path) );

        FileHelper::createDirectory($dir);

        if( $file->saveAs($path, $deleteTempFile) === false ) {
            throw new InvalidCallException(__METHOD__ . ": can not upload file in path {$dir}/{$name}");
        }

        /**
         * @var File $model
         */
        $model = Yii::createObject([
            'class'         => $className,
            'content_type'  => $file->type,
            'original_name' => $file->name,
            'path'          => "/public/upload/{$hash}/{$name}",
        ]);

        if( $model instanceof File === false ) {
            throw new InvalidParamException(__METHOD__ . ': $className argument should be class name of ' . self::class . ' or extending class');
        }

        return $model;
    }
}
