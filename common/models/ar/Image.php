<?php

namespace common\models\ar;

use Yii;
use yii\base\InvalidCallException;
use yii\web\UploadedFile;
use yii\caching\FileDependency;

/**
 * Class Image
 * @package common\models\ar
 */
class Image extends File
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return 'public.image';
    }

    const MIME_JPEG = 'image/jpeg';
    const MIME_GIF  = 'image/gif';
    const MIME_PNG  = 'image/png';
    const MIME_SVG  = 'image/svg+xml';

    public static $mimeTypes = [
        self::MIME_JPEG,
        self::MIME_GIF,
        self::MIME_PNG,
        self::MIME_SVG,
    ];

    /**
     * @return \common\models\aq\ImageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\aq\ImageQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['content_type'], 'in', 'range' => self::$mimeTypes],
        ]);
    }

    /**
     * @param UploadedFile $file
     * @param bool         $deleteTempFile
     * @param string       $className
     *
     * @return \common\models\ar\File
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\InvalidCallException
     * @throws \yii\base\InvalidParamException
     * @throws \yii\base\Exception
     */
    public static function upload(UploadedFile $file, $deleteTempFile = true, $className = Image::class)
    {
        if( in_array($file->type, self::$mimeTypes, true) === false ) {
            throw new InvalidCallException(__METHOD__ . ": try upload unexpected file type: {$file->type}");
        }

        return parent::upload($file, $deleteTempFile, $className);
    }

    /**
     * Thumb image generates by nginx with module 'ngx_http_image_filter_module'
     *
     * @param $width
     * @param $height
     *
     * @return string
     */
    public function getThumbUrl($width, $height = NULL)
    {
        if( $height === NULL ) {
            $height = $width;
        }

        return "/thumb/{$width}x{$height}" . $this->path;
    }

    /**
     * @return string
     * @throws \yii\base\InvalidParamException
     */
    public function base64()
    {
        $key    = 'image-base64-encode-' . $this->id;
        $base64 = Yii::$app->getCache()->get($key);

        if( $base64 === false ) {
            $path = Yii::getAlias('@frontend/web' . $this->path);

            if( file_exists($path) ) {
                $base64 = base64_encode(file_get_contents($path));
            }

            Yii::$app->getCache()->set($key, $base64, 0, new FileDependency([
                'fileName' => $path,
            ]));
        }

        return $base64;
    }
}
