<?php

namespace common\models\ar;

use yii\base\NotSupportedException;
use yii\web\IdentityInterface;
use common\behaviors\TimestampBehavior;
use common\db\ActiveRecord;
use common\models\aq\UserQuery;

/**
 * User model
 *
 * @property integer $id
 * @property integer $status
 * @property string  $name
 * @property string  $login
 * @property string  $email
 * @property string  $password_hash
 * @property string  $password_reset_token
 * @property string  $auth_key
 * @property string  $access_token
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property string  $statusLabel
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_ACTIVE   = 'active';
    const STATUS_INACTIVE = 'inactive';
    const STATUS_DELETED  = 'deleted';

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'public.user';
    }

    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    public static function findIdentityByAccessToken($token, $type = NULL)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * @return \common\models\aq\UserQuery
     */
    public static function find()
    {
        return new UserQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function extraBehaviors()
    {
        return [
            'insert' => [
                TimestampBehavior::class,
            ],
            'update' => [
                TimestampBehavior::class,
            ],
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        $statusLabels = self::statusLabels();

        return [
            [['status', 'name', 'login', 'email', 'password_hash', 'auth_key', 'access_token'], 'required'],
            [['status'], 'in', 'range' => array_keys($statusLabels)],
            [['email'], 'email'],
            [['login', 'email', 'password_reset_token', 'access_token'], 'unique'],
        ];
    }

    /**
     * @return array
     */
    public static function labels()
    {
        return [
            'id'                   => \Yii::t('app', 'ID'),
            'status'               => \Yii::t('app', 'Status'),
            'name'                 => \Yii::t('app', 'Name'),
            'login'                => \Yii::t('app', 'Login'),
            'email'                => \Yii::t('app', 'E-mail'),
            'auth_key'             => \Yii::t('app', 'Authorization key'),
            'password_hash'        => \Yii::t('app', 'Password hash'),
            'password_reset_token' => \Yii::t('app', 'Password reset token'),
            'access_token'         => \Yii::t('app', 'Access token'),
            'created_at'           => \Yii::t('app', 'Created at'),
            'updated_at'           => \Yii::t('app', 'Updated at'),
        ];
    }

    /**
     * @return array
     */
    public static function statusLabels()
    {
        return [
            self::STATUS_ACTIVE   => \Yii::t('app', 'Active'),
            self::STATUS_INACTIVE => \Yii::t('app', 'Inactive'),
            self::STATUS_DELETED  => \Yii::t('app', 'Deleted'),
        ];
    }

    /**
     * @return string
     */
    public function getStatusLabel()
    {
        $statusLabels = self::statusLabels();

        return isset($statusLabels[$this->status]) ? $statusLabels[$this->status] : NULL;
    }
}
