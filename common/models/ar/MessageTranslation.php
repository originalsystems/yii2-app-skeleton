<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "message_translation".
 *
 * @property integer       $id
 * @property string        $language
 * @property string        $translation
 *
 * @property MessageSource $messageSource
 */
class MessageTranslation extends \common\db\ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return 'message_translation';
    }

    /**
     * @return \common\models\aq\MessageTranslationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\aq\MessageTranslationQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['id', 'language'], 'required'],
            [['id'], 'integer'],
            [['translation'], 'string'],
            [['language'], 'string', 'max' => 16],
            [
                ['id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => MessageSource::className(),
                'targetAttribute' => ['id' => 'id'],
            ],
        ];
    }

    /**
     * @return array
     */
    public static function labels()
    {
        return [
            'id'          => Yii::t('app', 'ID'),
            'language'    => Yii::t('app', 'Language'),
            'translation' => Yii::t('app', 'Translation'),
        ];
    }

    /**
     * @return \common\models\aq\MessageSourceQuery
     */
    public function getMessageSource()
    {
        return $this->hasOne(MessageSource::className(), ['id' => 'id']);
    }
}
