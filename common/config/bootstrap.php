<?php

use yii\helpers\Url;
use yii\helpers\StringHelper;
use yii\helpers\Html;
use yii\helpers\Console;
use yii\helpers\ArrayHelper;

Yii::setAlias('api', APP_ROOT . '/api');
Yii::setAlias('backend', APP_ROOT . '/backend');
Yii::setAlias('console', APP_ROOT . '/console');
Yii::setAlias('common', APP_ROOT . '/common');
Yii::setAlias('frontend', APP_ROOT . '/frontend');
Yii::setAlias('stash', APP_ROOT . '/stash');
Yii::setAlias('upload', APP_ROOT . '/public/upload');
Yii::setAlias('node_modules', APP_ROOT . '/node_modules');

Yii::$classMap[ArrayHelper::class]  = '@common/yii/helpers/ArrayHelper.php';
Yii::$classMap[Console::class]      = '@common/yii/helpers/Console.php';
Yii::$classMap[Html::class]         = '@common/yii/helpers/Html.php';
Yii::$classMap[StringHelper::class] = '@common/yii/helpers/StringHelper.php';
Yii::$classMap[Url::class]          = '@common/yii/helpers/Url.php';
