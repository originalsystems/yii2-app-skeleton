<?php
/**
 * Local database configuration
 * (copy this to file w/o ".default" to use)
 */

return [
    'dsn'      => 'pgsql:host=localhost;dbname=dbname',
    'username' => 'username',
    'password' => 'password',
];
