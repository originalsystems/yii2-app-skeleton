<?php
/**
 * Local common configuration file
 * (copy this to file w/o ".default" to use)
 */

return [
    'components' => [
        'mailer' => [
            'class'            => \yii\swiftmailer\Mailer::class,
            'viewPath'         => '@common/mail',
            'useFileTransport' => true,
        ],
    ],
];
