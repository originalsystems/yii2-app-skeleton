<?php
/**
 * Local database configuration
 */

return \yii\helpers\ArrayHelper::merge([
    'class'               => 'yii\db\Connection',
    'dsn'                 => 'pgsql:host=localhost;dbname=dbname',
    'username'            => 'username',
    'password'            => 'password',
    'charset'             => 'utf8',
    'enableSchemaCache'   => true,
    'schemaCacheDuration' => 3600,
    'schemaCache'         => 'cache',
    'schemaMap'           => [
        'pgsql' => 'common\db\pgsql\Schema',
    ],
], file_exists(__DIR__ . '/db.local.php') ? require __DIR__ . '/db.local.php' : []);
