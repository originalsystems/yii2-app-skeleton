<?php
/**
 * Main common configuration file
 */

return \yii\helpers\ArrayHelper::merge([
    'components'     => [
        'assetManager' => [
            'appendTimestamp' => true,
        ],
        'authManager'  => [
            'class' => \common\rbac\AuthManager::class,
            'cache' => 'cache',
        ],
        'cache'        => [
            'class'     => \yii\redis\Cache::class,
            'redis'     => 'redis',
            'keyPrefix' => 'cache:',
        ],
        'curl'         => [
            'class' => \common\base\Curl::class,
        ],
        'db'           => require __DIR__ . '/db.php',
        'i18n'         => [
            'class'        => \common\i18n\I18N::class,
            'languages'    => [
                'en',
                'ru',
            ],
            'translations' => [
                'app*' => [
                    'class'              => \yii\i18n\DbMessageSource::class,
                    'forceTranslation'   => true,
                    'sourceMessageTable' => '"public"."message_source"',
                    'messageTable'       => '"public"."message_translation"',
                    'cachingDuration'    => 1800,
                    'enableCaching'      => YII_ENV_PROD,
                ],
            ],
        ],
        'mailer'       => [
            'class'    => \yii\swiftmailer\Mailer::class,
            'viewPath' => '@common/mail',
        ],
        'redis'        => [
            'class'    => \yii\redis\Connection::class,
            'port'     => 6379,
            'database' => 0,
        ],
        'session'      => [
            'class'     => \yii\redis\Session::class,
            'keyPrefix' => 'session:',
            'redis'     => 'redis',
            'timeout'   => 2592000,
        ],
    ],
    'language'       => 'en',
    'name'           => 'Yii2 app skeleton',
    'params'         => require __DIR__ . '/params.php',
    'sourceLanguage' => 'en',
    'vendorPath'     => APP_ROOT . '/vendor',
], file_exists(__DIR__ . '/main.local.php') ? require __DIR__ . '/main.local.php' : []);
