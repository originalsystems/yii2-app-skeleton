<?php

namespace common\base;

use yii\base\Model;
use common\mixin\ModelTrait;

class FormModel extends Model
{
    use ModelTrait;
}
