<?php

namespace common\base;

use yii\base\Component;
use yii\base\InvalidCallException;
use yii\helpers\ArrayHelper;

class Curl extends Component
{
    // Default options from config.php
    public $options = [];

    // request specific options - valid only for single request
    public $request_options = [];

    private $_header;
    private $_headerMap;

    private $_error     = '';
    private $_errno     = 0;
    private $_http_code = 0;
    private $_info      = [];

    // default config
    private $_config = [
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HEADER         => false,
        CURLOPT_VERBOSE        => false,
        CURLOPT_AUTOREFERER    => true,
        CURLOPT_CONNECTTIMEOUT => 30,
        CURLOPT_TIMEOUT        => 30,
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_USERAGENT      => 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)'
    ];

    public function getOptions()
    {
        $options = ArrayHelper::merge($this->request_options, $this->options, $this->_config);

        return $options;
    }

    public function setOption($key, $value, $default = false)
    {
        if( $default ) {
            $this->options[$key] = $value;
        } else {
            $this->request_options[$key] = $value;
        }

        return $this;
    }

    /**
     * Clears Options
     * This will clear only the request specific options. Default options cannot be cleared.
     *
     * @return static $this
     */
    public function resetOptions()
    {
        $this->request_options = [];

        return $this;
    }

    /**
     * Resets the Option to Default option
     *
     * @param string $key
     *
     * @return static $this
     */
    public function resetOption($key)
    {
        if( isset($this->request_options[$key]) ) {
            unset($this->request_options[$key]);
        }

        return $this;
    }

    /**
     * @param array $options
     * @param bool  $default
     *
     * @return static
     */
    public function setOptions($options, $default = false)
    {
        if( $default ) {
            $this->options = $options + $this->request_options;
        } else {
            $this->request_options = $options + $this->request_options;
        }

        return $this;
    }

    /**
     * @param string       $url
     * @param array|string $data
     *
     * @return string
     */
    public function buildUrl($url, $data = [])
    {
        if( preg_match('/^(?:https?|ftp):\/\//', $url) !== 1 ) {
            $url = 'http://' . $url;
        }

        if( empty($data) ) {
            return $url;
        }

        $parsed = parse_url($url);

        isset($parsed['query']) ? parse_str($parsed['query'], $parsed['query']) : $parsed['query'] = [];

        if( is_array($data) ) {
            $params          = isset($parsed['query']) ? $data + $parsed['query'] : $data;
            $parsed['query'] = ($params) ? '?' . http_build_query($params) : '';
        } elseif( is_string($data) && !empty($data) ) {
            $parsed['query'] = '?' . $data;
        }

        $parsed['query'] = isset($parsed['query'])
            ? (is_array($parsed['query']) ? '?' . http_build_query($parsed['query']) : $parsed['query'])
            : '';
        $parsed['path']  = isset($parsed['path']) ? $parsed['path'] : '/';
        $parsed['port']  = isset($parsed['port']) ? ':' . $parsed['port'] : '';

        return $parsed['scheme'] . '://' . $parsed['host'] . $parsed['port'] . $parsed['path'] . $parsed['query'];
    }

    /**
     * @param string $url
     * @param array  $options
     *
     * @return mixed
     */
    private function exec($url, $options)
    {
        $ch = curl_init($url);

        $this->_header    = NULL;
        $this->_headerMap = NULL;

        $this->_info      = curl_getinfo($ch);
        $this->_error     = '';
        $this->_errno     = 0;
        $this->_http_code = 0;

        curl_setopt_array($ch, $options);

        $output = curl_exec($ch);

        $this->_http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        $this->_error = curl_error($ch);
        $this->_errno = curl_errno($ch);
        $this->_info  = curl_getinfo($ch);

        if( isset($options[CURLOPT_HEADER]) && $options[CURLOPT_HEADER] == true ) {
            list($header, $output) = $this->processHeader($output, curl_getinfo($ch, CURLINFO_HEADER_SIZE));
            $this->_header = $header;
        }
        curl_close($ch);

        return $output;
    }

    private function processHeader($response, $header_size)
    {
        return [substr($response, 0, $header_size), substr($response, $header_size)];
    }

    /**
     * @param string       $name
     * @param string       $url
     * @param string|array $data
     *
     * @return mixed
     */
    public function method($name, $url, $data)
    {
        if( $this->hasMethod($name) === false ) {
            throw new InvalidCallException(__CLASS__ . ": method \"{$name}\" does not exists.");
        }

        return $this->{$name}($url, $data);
    }

    /**
     * @param string $url
     * @param array  $params
     *
     * @return mixed
     */
    public function get($url, $params = [])
    {
        $url     = $this->buildUrl($url, $params);
        $options = $this->getOptions();

        return $this->exec($url, $options);
    }

    /**
     * @param string $url
     * @param string $data
     * @param array  $params
     *
     * @return mixed
     */
    public function post($url, $data, $params = [])
    {
        $url = $this->buildUrl($url, $params);

        $options               = $this->getOptions();
        $options[CURLOPT_POST] = true;

        if( is_array($data) ) {
            $options[CURLOPT_POSTFIELDS] = http_build_query($data);
        } else {
            $options[CURLOPT_POSTFIELDS] = $data;
        }

        return $this->exec($url, $options);
    }

    /**
     * @param string $url
     * @param string $data
     * @param array  $params
     *
     * @return mixed
     */
    public function put($url, $data = NULL, $params = [])
    {
        $url = $this->buildUrl($url, $params);

        $f = fopen('php://temp', 'rw+');
        fwrite($f, $data);
        rewind($f);

        $options                     = $this->getOptions();
        $options[CURLOPT_PUT]        = true;
        $options[CURLOPT_INFILE]     = $f;
        $options[CURLOPT_INFILESIZE] = strlen($data);

        return $this->exec($url, $options);
    }

    /**
     * @param string $url
     * @param array  $data
     * @param array  $params
     *
     * @return mixed
     */
    public function patch($url, $data = [], $params = [])
    {
        $url = $this->buildUrl($url, $params);

        $options                        = $this->getOptions();
        $options[CURLOPT_CUSTOMREQUEST] = 'PATCH';
        $options[CURLOPT_POSTFIELDS]    = $data;

        return $this->exec($url, $options);
    }

    /**
     * @param string $url
     * @param array  $params
     *
     * @return mixed
     */
    public function delete($url, $params = [])
    {
        $url = $this->buildUrl($url, $params);

        $options                        = $this->getOptions();
        $options[CURLOPT_CUSTOMREQUEST] = 'DELETE';

        return $this->exec($url, $options);
    }

    /**
     * Gets header of the last curl call if header was enabled
     *
     * @return array
     */
    public function getHeaders()
    {
        if( !$this->_header ) {
            return [];
        }

        if( !$this->_headerMap ) {

            $headers               = explode("\r\n", trim($this->_header));
            $output                = [];
            $output['http_status'] = array_shift($headers);

            foreach( $headers as $line ) {
                $params = explode(':', $line, 2);

                if( !isset($params[1]) ) {
                    $output['http_status'] = $params[0];
                } else {
                    $output[trim($params[0])] = trim($params[1]);
                }
            }

            $this->_headerMap = $output;
        }

        return $this->_headerMap;
    }

    /**
     * @param array $header
     *
     * @return static
     */
    public function addHeader($header = [])
    {
        $h = isset($this->request_options[CURLOPT_HTTPHEADER]) ? $this->request_options[CURLOPT_HTTPHEADER] : [];
        foreach( $header as $k => $v ) {
            $h[] = $k . ': ' . $v;
        }

        $this->setHeaders($h);

        return $this;
    }

    /**
     * @param string $key
     *
     * @return string|null
     */
    public function getHeader($key)
    {
        $headers = array_change_key_case($this->getHeaders(), CASE_LOWER);
        $key     = strtolower($key);

        return isset($headers[$key]) ? $headers[$key] : NULL;
    }

    /**
     * @param array $header
     * @param bool  $default
     *
     * @return static
     */
    public function setHeaders($header = [], $default = false)
    {
        if( ArrayHelper::isAssociative($header) ) {
            $out = [];
            foreach( $header as $k => $v ) {
                $out[] = $k . ': ' . $v;
            }
            $header = $out;
        }

        $this->setOption(CURLOPT_HTTPHEADER, $header, $default);

        return $this;
    }

    /**
     * @return string
     */
    public function getError()
    {
        return $this->_error;
    }

    /**
     * @return integer
     */
    public function getErrno()
    {
        return (int)$this->_errno;
    }

    /**
     * @return integer
     */
    public function getHttpCode()
    {
        return (int)$this->_http_code;
    }

    /**
     * @return array
     */
    public function getInfo()
    {
        return $this->_info;
    }
}
