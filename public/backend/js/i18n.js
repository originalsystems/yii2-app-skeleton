jQuery(function() {
    "use strict";

    (function() {
        var $container   = jQuery('.section-i18n-grid'),
            $pjax        = jQuery('#i18n-grid-pjax'),
            $filter_form = jQuery('#i18n-grid-filter'),
            $modal       = $container.find('.modal'),
            $modal_form  = $modal.find('form');

        if( $container.length === 0 ) {
            return;
        }

        $container.on('click', 'table tr[data-key] [data-role="edit"]', function(e) {
            e.preventDefault();

            var $this = jQuery(this),
                $tr   = $this.closest('tr'),
                id    = $tr.data('key');

            jQuery.ajax({
                url:      '/i18n/view',
                data:     {id: id},
                dataType: 'json',
                type:     'get',
                success:  function(json) {
                    $modal_form.find('[data-bind="id"]').val(json.id);
                    $modal_form.find('[data-bind="source"]').text(json.source);

                    for( var language in json.translations ) {
                        $modal_form.find('[data-bind="translation"][data-language="' + language + '"]').val(json.translations[language]);
                    }

                    $modal.modal('show');
                }
            });
        });

        $filter_form.on('beforeSubmit', function(e) {
            var url   = location.origin + location.pathname,
                query = App.search_values_with_form($filter_form);

            if( Object.keys(query).length !== 0 ) {
                url += '?' + decodeURIComponent(jQuery.param(query));
            }

            jQuery.pjax({url: url, container: $pjax.selector});

            return false;
        });

        $filter_form.on('keyup', 'input, textarea', function(e) {
            if( e.which === 13 ) {
                $filter_form.submit();
            }
        });

        $modal_form.on('beforeSubmit', function(e) {
            jQuery.ajax({
                url:      $modal_form.attr('action'),
                data:     $modal_form.serialize(),
                dataType: 'json',
                type:     'post',
                success:  function(json) {
                    $modal.modal('hide');
                    jQuery('#i18n-grid').yiiGridView('applyFilter');
                }
            });

            return false;
        });

        if( window.autosize ) {
            autosize($modal.find('form textarea'));

            $modal.on('shown.bs.modal', function() {
                autosize.update($modal.find('form textarea'));
            });
        }
    })();
});
