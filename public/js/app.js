var App = (function() {
    "use strict";

    return {
        search_values:           function(only, except) {
            var values = [];

            if( location.search !== '' ) {
                values = decodeURIComponent(location.search.substr(1)).split('&').map(function(item) {
                    var name = item.split('=')[0];

                    if( Array.isArray(only) && only.indexOf(name) === -1 ) {
                        return null;
                    }

                    if( Array.isArray(except) && except.indexOf(name) !== -1 ) {
                        return null;
                    }

                    return {
                        name:  name,
                        value: item.split('=')[1]
                    };
                }).filter(function(item) {return !!item;});
            }

            return values;
        },
        /**
         * @param {string|Element|jQuery} form
         * @return {Object}
         */
        search_values_with_form: function(form) {
            var $form  = jQuery(form),
                names  = [],
                query  = {},
                values = $form.serializeArray();

            $form.find('[name]').each(function() {
                var $this = jQuery(this);

                names.push($this.attr('name'));
            });

            values = values.concat(this.search_values(null, names));

            values.forEach(function(item) {
                if( /\[\d*\]$/.test(item.name) ) {
                    var name = item.name.replace(/\[\d*\]$/, '');

                    if( Array.isArray(query[name]) === false ) {
                        query[name] = [];
                    }

                    if( query[name].indexOf(item.value) === -1 ) {
                        query[name].push(item.value);
                    }
                } else {
                    query[item.name] = item.value;
                }
            });

            Object.keys(query).forEach(function(key) {
                if( query[key] === '' ) {
                    delete query[key];
                }
            });

            return query;
        },

        number_format: function(number) {
            var string = number + '',
                out    = '';

            string.split('').reverse().forEach(function(num, i) {
                if( i !== 0 && i % 3 === 0 ) {
                    out = ' ' + out;
                }

                out = num + out;
            });

            return out;
        }
    };
})();
