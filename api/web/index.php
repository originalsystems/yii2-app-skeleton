<?php

require dirname(dirname(__DIR__)) . '/common/config/define.php';

require APP_ROOT . '/vendor/autoload.php';
require APP_ROOT . '/common/Yii.php';
require APP_ROOT . '/common/config/bootstrap.php';
require APP_ROOT . '/api/config/bootstrap.php';

$config = require APP_ROOT . '/api/config/main.php';

$application = new common\web\Application($config);
$application->run();
