<?php

namespace api\controllers;

use Yii;
use yii\web\BadRequestHttpException;
use common\rest\Controller;
use common\web\Response;

class ErrorController extends Controller
{
    public    $authenticate         = false;
    public    $wrapResponse         = false;
    protected $authenticateOptional = ['index'];

    public function actionIndex()
    {
        Yii::$app->getResponse()->format = Response::FORMAT_JSON;

        $exception = Yii::$app->getErrorHandler()->exception ? : new BadRequestHttpException();

        return Yii::$app->getErrorHandler()->convertExceptionToArray($exception);
    }
}
