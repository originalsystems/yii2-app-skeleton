<?php

namespace api\controllers;

use common\rest\Controller;

class DefaultController extends Controller
{
    public $authenticate = false;

    public function actionIndex()
    {
        return 'success';
    }
}
