<?php
/**
 * Api url manager rules
 */

return [
    'PUT,PATCH <controller:[\w-]+>/<id:\d+>' => '<controller>/update',
    'DELETE <controller:[\w-]+>/<id:\d+>'    => '<controller>/delete',
    'GET,HEAD <controller:[\w-]+>/<id:\d+>'  => '<controller>/view',
    'POST <controller:[\w-]+>'               => '<controller>/create',
    'GET,HEAD <controller:[\w-]+>'           => '<controller>/index',
    '<controller:[\w-]+>/<id:\d+>'           => '<controller>/options',
    '<controller:[\w-]+>'                    => '<controller>/options',
];
