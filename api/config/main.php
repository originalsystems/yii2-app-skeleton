<?php
/**
 * Main api configuration file
 */

return \yii\helpers\ArrayHelper::merge(require APP_ROOT . '/common/config/main.php', [
    'id'                  => 'app-api',
    'basePath'            => dirname(__DIR__),
    'controllerNamespace' => 'api\controllers',
    'bootstrap'           => ['log'],
    'defaultRoute'        => 'default/index',
    'components'          => [
        'user'         => [
            'class'           => \common\web\User::class,
            'identityClass'   => \common\models\ar\User::class,
            'enableSession'   => false,
            'enableAutoLogin' => false,
            'loginUrl'        => NULL,
        ],
        'errorHandler' => [
            'class'       => \common\rest\ErrorHandler::class,
            'errorAction' => 'error/index',
        ],
        'response'     => [
            'class' => \common\web\Response::class,
        ],
        'request'      => [
            'class'                  => \common\web\Request::class,
            'enableCookieValidation' => false,
            'enableCsrfValidation'   => false,
            'enableCsrfCookie'       => false,
        ],
        'log'          => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'  => \yii\log\FileTarget::class,
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'urlManager'   => [
            'enablePrettyUrl'     => true,
            'enableStrictParsing' => true,
            'showScriptName'      => false,
            'rules'               => require __DIR__ . '/rules.php',
        ],
    ],
], file_exists(__DIR__ . '/main.local.php') ? require __DIR__ . '/main.local.php' : []);
