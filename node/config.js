var merge    = require('merge'),
    original = require('./config/main.json'),
    local    = require('./config/main.local.json');

module.exports = merge.recursive(true, original, local);
