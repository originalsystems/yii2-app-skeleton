#! /bin/bash

PROJECT_DIR=$(dirname $(dirname $(readlink -e ${0})))
PROJECT_NAME=${PROJECT_DIR##*/}
PROJECT_NAME=${PROJECT_NAME/-/_}

DEFAULT_LOG_DIR="/var/log/nginx"
DEFAULT_PHP_LISTEN="127.0.0.1:9000"

# -- collect some info

for DEFAULT_THUMB_PORT in `seq 8085 8150`; do
    if [ `netstat -nl | grep ":${DEFAULT_THUMB_PORT}"  | wc -l` -eq 0 ]; then
        break;
    fi
done

read -p "Nginx config absolute path (required): " -e READ_NGINX_CONFIG
if [ -z ${READ_NGINX_CONFIG} ];
then
    echo 'This field cannot be empty!';
    exit 1;
fi;

NGINX_DIR=$(dirname $(dirname ${READ_NGINX_CONFIG}));

read -p "Project hostname (required): " -e READ_HOST
if [ -z ${READ_HOST} ];
then
    echo 'This field cannot be empty!';
    exit 1;
fi;

read -p "Enter HTTP port for thumb generator [${DEFAULT_THUMB_PORT}]: " -e READ_THUMB_PORT
if [ -z ${READ_THUMB_PORT} ];
    then
        READ_THUMB_PORT=${DEFAULT_THUMB_PORT};
fi;

read -p "Enter php listener, ip address with port or path to socket [${DEFAULT_PHP_LISTEN}]: " -e READ_PHP_LISTEN
if [ -z ${READ_PHP_LISTEN} ];
    then
        READ_PHP_LISTEN=${DEFAULT_PHP_LISTEN};
fi;

read -p "Enter nginx log absolute path [${DEFAULT_LOG_DIR}]: " -e READ_LOG_DIR
if [ -z ${READ_LOG_DIR} ];
    then
        READ_LOG_DIR=${DEFAULT_LOG_DIR};
    else
        READ_LOG_DIR=`echo ${READ_LOG_DIR} | perl -pe 's,/+$,,'`;
fi;

# -- do some post collect things

echo -e "\nCreating log dirs...";

LOG_DIR="${READ_LOG_DIR}/${READ_HOST}"
if ! sudo [ -d "${LOG_DIR}" ];
then
    echo -e "\nCreating log dir (${LOG_DIR})...\n";
    sudo mkdir -p ${LOG_DIR};
else
    echo -e "\nLog dir (${LOG_DIR}) already exists, nothing to do...\n";
fi;

# -- replace some placeholders in tpl config and place it to place :)

sed \
-e "/^#/! s,#DIR#,${PROJECT_DIR},g" \
-e "/^#/! s,#NAME#,${PROJECT_NAME},g" \
-e "/^#/! s,#HOST#,${READ_HOST},g" \
-e "/^#/! s,#PHP_LISTEN#,${READ_PHP_LISTEN},g" \
-e "/^#/! s,#THUMB_PORT#,${READ_THUMB_PORT},g" \
${PROJECT_DIR}/stash/nginx_vhost.tpl \
| \
sudo tee ${READ_NGINX_CONFIG} 1>/dev/null 2>&1
