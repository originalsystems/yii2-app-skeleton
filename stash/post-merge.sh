#! /bin/bash

# check that composer path is valid
COMPOSER="/usr/local/bin/composer"

# check project directory
PROJECT_DIR=$(dirname $(dirname $(dirname $(readlink -e ${0}))));

cd ${PROJECT_DIR};

# remove runtime and assets files
php ${COMPOSER} run-script clear

# vendor update
php ${COMPOSER} install

# node_modules update
npm install

# trigger migration mechanism
php yii migrate --interactive=0

# flush all cashes
php yii cache/flush-all

cd -
