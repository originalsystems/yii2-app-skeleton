#! /bin/bash

STASH_DIR=$(dirname $(readlink -e ${0}))
PROJECT_DIR=$(dirname ${STASH_DIR})

read -p "Project name for idea (required): " -e PROJECT_NAME
if [ -z ${PROJECT_NAME} ]; then
    echo "This field cannot be empty!";
fi;

if [ ! -d "${PROJECT_DIR}/.idea" ]; then
    mkdir "${PROJECT_DIR}/.idea"
fi

echo "${PROJECT_NAME}" > "${PROJECT_DIR}/.idea/.name"
cp "${STASH_DIR}/idea/project.iml" "${PROJECT_DIR}/.idea/${PROJECT_NAME}.iml"
cp "${STASH_DIR}/idea/sqldialects.xml" "${PROJECT_DIR}/.idea/sqldialects.xml"
sed -e "s/#PROJECT_NAME#/${PROJECT_NAME}/g" "${STASH_DIR}/idea/modules.xml" > "${PROJECT_DIR}/.idea/modules.xml"
